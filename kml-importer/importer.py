from pykml import parser as kmlparser
import argparse
import json

def main():
    parser = argparse.ArgumentParser(description='Parse KML and output points')
    parser.add_argument('filename', metavar='F', type=str, nargs='+', help='KML file to parse')
    args = parser.parse_args()
    filename = args.filename[0]
    if not filename.endswith('.kml'):
        exit('Unsupported file extension, supported : .kml')
    result = []
    with open(filename, 'r', encoding='utf-8') as f:
        kml = kmlparser.parse(f).getroot()
        for point in kml.Document.Folder.Placemark:
            coords = point.Point.coordinates.text.strip()
            table = coords.maketrans(' ', ' ')
            result.append({'name': point.name.text,
                           'coords': coords.translate(table)})

    with open('kml.json', 'w', encoding='utf-8') as f:
        json.dump(result, f, ensure_ascii=False, indent=2)


if __name__ == "__main__":
    main()
