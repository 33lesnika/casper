FROM amd64/openjdk:15-buster as build

WORKDIR /build

ARG LOGIN
ARG PASSWORD
ARG DEPENDENCY=target/dependency

COPY .mvn .mvn
COPY mvnw mvnw
COPY pom.xml pom.xml

# RUN ./mvnw -Drepo.login=$LOGIN -Drepo.pwd=$PASSWORD dependency:go-offline -B -s settings.xml

COPY src src
COPY maven-repository maven-repository

RUN ./mvnw -DskipTests clean package \
        && mkdir -p $DEPENDENCY \
        && (cd $DEPENDENCY; jar -xf ../*.jar)


FROM amd64/openjdk:15-buster

VOLUME /tmp

EXPOSE 8080
ARG DEPENDENCY=/build/target/dependency
ARG HEAPDUMP_DIR=/app/dump
ENV TZ="Asia/Almaty"
ENV JAVA_OPTS="-Dhibernate.types.print.banner=false -Djava.security.egd=file:/dev/./urandom -XX:+ExitOnOutOfMemoryError -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=$HEAPDUMP_DIR -Dspring.profiles.active=docker"
COPY --from=build $DEPENDENCY/BOOT-INF/lib /app/lib
COPY --from=build $DEPENDENCY/META-INF /app/META-INF
COPY --from=build $DEPENDENCY/BOOT-INF/classes /app
ENTRYPOINT java $JAVA_OPTS -cp app:/app/lib/* xyz.lesnik.rabbittest.RabbitTestApplication
