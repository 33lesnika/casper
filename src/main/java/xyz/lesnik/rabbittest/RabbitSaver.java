package xyz.lesnik.rabbittest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.*;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.bson.RawBsonDocument;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import xyz.lesnik.rabbittest.config.ConsumerConfig;
import xyz.lesnik.rabbittest.config.RabbitConfig;
import xyz.lesnik.rabbittest.entity.Axle;
import xyz.lesnik.rabbittest.entity.DataSourceSettings;
import xyz.lesnik.rabbittest.entity.Image;
import xyz.lesnik.rabbittest.entity.RecognitionEvent;
import xyz.lesnik.rabbittest.model.RecognitionImageData;
import xyz.lesnik.rabbittest.model.RecognitionModel;
import xyz.lesnik.rabbittest.repository.DataSourceSettingsRepository;
import xyz.lesnik.rabbittest.service.DataSourceChangeEvent;
import xyz.lesnik.rabbittest.service.ImageService;
import xyz.lesnik.rabbittest.service.RecognitionEventService;

import javax.transaction.Transactional;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.TimeoutException;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 24.12.2020
 * RabbitTest
 * 16:25
 */

@Component
@Slf4j
public class RabbitSaver implements CommandLineRunner, ApplicationListener<DataSourceChangeEvent> {

    @Value("${rabbit.message.ignore-error:false}")
    @Getter
    @Setter
    private boolean ignoreError = false;

    private final RabbitConfig rabbitConfig;
    private final ObjectMapper mapper;
    private final ImageService imageService;
    private final RecognitionEventService recognitionEventService;
    private final DataSourceSettingsRepository dataSourceSettingsRepository;
    private final RabbitConnectionHolder rabbitConnectionHolder;

    private final PropertyChangeSupport support;

    public RabbitSaver(RabbitConfig rabbitConfig, ObjectMapper mapper, ImageService imageService, RecognitionEventService recognitionEventService, DataSourceSettingsRepository dataSourceSettingsRepository, RabbitConnectionHolder rabbitConnectionHolder) {
        this.rabbitConfig = rabbitConfig;
        this.mapper = mapper;
        this.imageService = imageService;
        this.recognitionEventService = recognitionEventService;
        this.dataSourceSettingsRepository = dataSourceSettingsRepository;
        this.rabbitConnectionHolder = rabbitConnectionHolder;
        support = new PropertyChangeSupport(this);
    }

    public void addPropertyChangeListener(PropertyChangeListener pcl) {
        support.addPropertyChangeListener(pcl);
    }

    public void removePropertyChangeListener(PropertyChangeListener pcl) {
        support.removePropertyChangeListener(pcl);
    }

    @Override
    public void run(String... args) throws Exception {
        new Thread(() -> {
            boolean started = false;
            while (!started) {
                try {
                    startRabbit();
                    started = true;
                } catch (Exception e) {
                    log.warn("Unable to start RabbitMQ connection");
                }
            }
        }).start();
    }

    private void startRabbit() {
        dataSourceSettingsRepository.findAll().forEach(this::startConnection);
    }

    private void startConnection(DataSourceSettings ds) {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUsername(ds.getUsername());
        factory.setPassword(ds.getPassword());
        factory.setHost(ds.getHost());
        factory.setPort(ds.getPort());
        ConsumerConfig config = new ConsumerConfig();
        config.setQueueName(ds.getQueue());
        config.setRoutingKey(ds.getRoutingKey());
        try {
            Connection conn = factory.newConnection();
            startConsumer(conn, config, ds.getAlias());
            rabbitConnectionHolder.store(ds.getId(), conn);
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }
    }

    @SneakyThrows
    private void startConsumer(Connection conn, ConsumerConfig cc, String alias) {
        log.info("SAVER: starting consumer for alias {} with config {}", alias, cc);
        Channel channel = conn.createChannel();
        String queueName = StringUtils.hasText(cc.getQueueName()) ? cc.getQueueName() : channel.queueDeclare().getQueue();
//        channel.queueBind(queueName, cc.getExchange(), cc.getRoutingKey());

        channel.basicConsume(queueName, false, "casperConsumerTag" + queueName,
                new DefaultConsumer(channel) {
                    @Override
                    public void handleDelivery(String consumerTag,
                                               Envelope envelope,
                                               AMQP.BasicProperties properties,
                                               byte[] body)
                            throws IOException {
                        long deliveryTag = envelope.getDeliveryTag();
                        RawBsonDocument doc = new RawBsonDocument(body);
                        if (log.isTraceEnabled()) {
                            final Path filePath = Path.of("files", System.currentTimeMillis() + ".json");
                            Files.writeString(filePath, doc.toJson());
                            log.trace("EVENT written to {}", filePath.toAbsolutePath());
                        }
                        var recognitionModel = mapper.readValue(doc.toJson(), RecognitionModel.class);
                        var event = toRecognitionEvent(recognitionModel);
                        if (log.isTraceEnabled()) {
                            final Path filePath = Path.of("files", System.currentTimeMillis() + ".event.json");
                            Files.writeString(filePath, mapper.writeValueAsString(event));
                            log.trace("PARSED_EVENT written to {}", filePath.toAbsolutePath());
                        }
                        if (StringUtils.hasText(alias)) {
                            event.setAlias(alias);
                        }
                        try {
                            recognitionEventService.create(event);
                            channel.basicAck(deliveryTag, false);
                        } catch (Exception e) {
                            if (!ignoreError) throw e;
                            log.error("Cannot save event {}", event);
                        }
                    }
                });
    }

    private RecognitionEvent toRecognitionEvent(RecognitionModel model) {
        RecognitionEvent event = new RecognitionEvent();
        event.setAlias(model.getAlias());
        event.setAxlesCount(model.getAxlesCount());
        event.setClassKZ(model.getClassKZ());
        event.setClassSick(model.getClassSick());
        event.setClassWIM(model.getClassWIM());
        event.setDirection(model.getDirection());
        event.setErrorFlag(model.getErrorFlag());
        event.setGrnz(model.getGrnz());
        event.setGrossWeight(model.getGrossWeight());
        if (model.getAnprfront() != null) {
            Image plateImage = fromBinary(model.getAnprfront().getBinary());
            event.setAnprfrontId(plateImage.getId());
            imageService.create(plateImage);
        }
        if (model.getAnprrear() != null) {
            Image plateImage = fromBinary(model.getAnprrear().getBinary());
            event.setAnprrearId(plateImage.getId());
            imageService.create(plateImage);
        }
        if (model.getOvcfront() != null) {
            Image plateImage = fromBinary(model.getOvcfront().getBinary());
            event.setOvcfrontId(plateImage.getId());
            imageService.create(plateImage);
        }
        if (model.getPlatefront() != null) {
            Image plateImage = fromBinary(model.getPlatefront().getBinary());
            event.setPlatefrontId(plateImage.getId());
            imageService.create(plateImage);
        }
        if (model.getPlaterear() != null) {
            Image plateImage = fromBinary(model.getPlaterear().getBinary());
            event.setPlaterearId(plateImage.getId());
            imageService.create(plateImage);
        }
        event.setName(model.getName());
        if (model.getQuality() != null) {
            event.setQuality(Integer.valueOf(model.getQuality()));
        }
//        "weightAxle": [
//        970.0,
//                880.0
//  ],
//        "distanceAxle": [
//        0.0,
//                2.72
//  ],
        int weightLength = 0;
        if (model.getWeightAxle() != null) {
            weightLength = model.getWeightAxle().size();
        }
        int distanceLength = 0;
        if (model.getDistanceAxle() != null) {
            distanceLength = model.getDistanceAxle().size();
        }
        var maxLength = Math.max(weightLength, distanceLength);
        List<Axle> axles = new ArrayList<>(maxLength);
        for (int i = 0; i < maxLength; i++) {
            Axle axle = new Axle();
            axle.setNumber(i);
            axles.add(axle);
        }
        for (int i = 0; model.getWeightAxle() != null && i < model.getWeightAxle().size(); i++) {
            axles.get(i).setWeight(model.getWeightAxle().get(i));
        }
        for (int i = 0; model.getDistanceAxle() != null && i < model.getDistanceAxle().size(); i++) {
            axles.get(i).setDistance(model.getDistanceAxle().get(i));
        }
        event.setAxles(axles);
        event.setTimestamp(model.getTimestamp());
        event.setVehicleHeight(model.getVehicleHeight());
        event.setVehicleLength(model.getVehicleLength());
        event.setVehicleWidth(model.getVehicleWidth());
        event.setVelocity(model.getVelocity());
        event.setWarningFlag(model.getWarningFlag());
        event.setWheelBase(model.getWheelBase());
        return event;
    }

    private static Image fromBinary(RecognitionImageData data) {
        byte[] modelPhoto = Base64.getDecoder().decode(data.getBase64());
        UUID modelPlateId = UUID.nameUUIDFromBytes(modelPhoto);
        Image image = new Image();
        image.setData(modelPhoto);
        image.setId(modelPlateId);
        return image;
    }

    @Override
    public void onApplicationEvent(DataSourceChangeEvent event) {
        restart();
    }

    public void restart() {
        rabbitConnectionHolder.values().forEach(c -> {
            try {
                c.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        rabbitConnectionHolder.clearAll();
        startRabbit();
    }

    @Scheduled(fixedRateString = "PT30S", initialDelayString = "PT30S")
    @Transactional
    public void checkConnectionsHealth() {
        log.debug("Started rabbit health check task");
        final Map<Long, DataSourceSettings> dssMap = dataSourceSettingsRepository.findAllBy()
                .collect(Collectors.toMap(DataSourceSettings::getId, Function.identity()));
        final List<DataSourceSettings> toStart = new ArrayList<>();
        rabbitConnectionHolder.forEach((id, conn) -> {
            if (!conn.isOpen()) {
                if (!dssMap.containsKey(id)) {
                    return;
                }
                toStart.add(dssMap.get(id));
            }
        });
        dssMap.keySet().stream()
                .filter(x -> rabbitConnectionHolder.get(x) == null)
                .forEach(x -> toStart.add(dssMap.get(x)));
        if (!toStart.isEmpty()) {
            log.info("SCHEDULED: Restarting rabbit listeners for configs: {}",
                    toStart.stream().map(Object::toString).collect(Collectors.joining(",")));
        }
        toStart.forEach(this::startConnection);
    }
}
