package xyz.lesnik.rabbittest.model;

import lombok.Data;
import xyz.lesnik.rabbittest.entity.SendStatus;

/**
 * 27.01.2022
 * SendStatusCount
 * 20:17
 */
@Data
public class SendStatusCount {
    SendStatus sendStatus;
    Long count;
}
