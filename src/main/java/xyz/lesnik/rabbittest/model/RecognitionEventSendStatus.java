package xyz.lesnik.rabbittest.model;

import lombok.Data;
import xyz.lesnik.rabbittest.entity.SendStatus;

/**
 * 05.02.2022
 * RecognitionEventSendStatus
 * 01:20
 */
@Data
public class RecognitionEventSendStatus {
    final String id;
    final SendStatus sendStatus;
}
