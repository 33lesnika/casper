package xyz.lesnik.rabbittest.model;

import lombok.Data;
import xyz.lesnik.rabbittest.entity.SendStatus;

/**
 * 25.01.2022
 * RecognitionEventSendStatusStatistics
 * 15:15
 */
@Data
public class RecognitionEventSendStatusStatistics {
    SendStatus status = SendStatus.NOT_SENT;
    long count = 0;
}
