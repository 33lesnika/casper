package xyz.lesnik.rabbittest.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;


/**
 * 25.12.2020
 * RecognitionModel
 * 0:32
 */
@Data
public class RecognitionModel {
    String name;
    String alias;
    @JsonProperty("GRNZ")
    String grnz;
    Long timestamp;
    String quality;
    RecognitionImage anprfront;
    RecognitionImage ovcfront;
    RecognitionImage platefront;
    RecognitionImage anprrear;
    RecognitionImage platerear;
    Double velocity = 0.0;
    @JsonProperty("axlescount")
    Integer axlesCount;
    @JsonProperty("errorflag")
    String errorFlag;
    @JsonProperty("warningflag")
    Integer warningFlag;
    Integer direction;
    @JsonProperty("grossweight")
    Double grossWeight;
    @JsonProperty("classwim")
    Integer classWIM;
    @JsonProperty("classsick")
    String classSick;
    @JsonProperty("classKZ")
    Integer classKZ;
    @JsonProperty("wheelbase")
    Double wheelBase;
    @JsonProperty("vehiclelength")
    Double vehicleLength = 0.0;
    @JsonProperty("vehicleheight")
    Double vehicleHeight = 0.0;
    @JsonProperty("vehiclewidth")
    Double vehicleWidth = 0.0;
    @JsonProperty("weightAxle")
    List<Double> weightAxle;
    @JsonProperty("distanceAxle")
    List<Double> distanceAxle;
}
