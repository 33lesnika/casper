package xyz.lesnik.rabbittest.model;

import lombok.Data;

/**
 * 25.12.2020
 * RecognitionImageData
 * 0:59
 */
@Data
public class RecognitionImageData {
    String base64;
    String subType;
}
