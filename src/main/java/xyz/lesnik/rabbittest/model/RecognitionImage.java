package xyz.lesnik.rabbittest.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 25.12.2020
 * RecognitionImage
 * 0:58
 */
@Data
public class RecognitionImage {
    @JsonProperty("$binary")
    private RecognitionImageData binary;
}
