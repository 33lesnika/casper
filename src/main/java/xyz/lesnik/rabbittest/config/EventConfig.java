package xyz.lesnik.rabbittest.config;

import lombok.Data;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 07.09.2021
 * EventConfig
 * 19:40
 */
@Data
public class EventConfig {
    boolean debug = false;
    String path = "files";
    String invalidFilesPath = "invalid";
    boolean filterEnabled = false;
    int weightMin = 4000;
    double speedMin = 5.0;
    int photosMin = 4;
    List<String> include = new ArrayList<>();
    Set<String> bypassArchIds = new HashSet<>();
}
