package xyz.lesnik.rabbittest.config;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 07.09.2021
 * AppConfigProperties
 * 19:36
 */
@Component
@ConfigurationProperties("app")
@Data
@Slf4j
public class AppConfigProperties {
    EventConfig events = new EventConfig();

    @PostConstruct
    public void init(){
        log.info("App config: {}", this);
    }
}
