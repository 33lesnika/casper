package xyz.lesnik.rabbittest.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

/**
 * 31.12.2021
 * ArchMapperConfig
 * 13:26
 */
@Configuration
@ConfigurationProperties("arch")
@Data
public class ArchMapperConfig {
    private Map<String, String> names;
}
