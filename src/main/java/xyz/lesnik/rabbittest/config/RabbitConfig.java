package xyz.lesnik.rabbittest.config;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * 30.12.2020
 * RabbitConfig
 * 13:16
 */
@ConfigurationProperties("rabbit")
@Configuration
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Slf4j
public class RabbitConfig {
    String username = "guest";
    String password = "guest";
    String host = "localhost";
    short port = 5672;

    List<ConsumerConfig> consumers;

    @PostConstruct
    public void init(){
        log.info(toString());
    }
}
