package xyz.lesnik.rabbittest.config;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;

/**
 * 14.12.2020
 * MongoConfig
 * 22:11
 */

@Configuration
public class MongoConfig extends AbstractMongoClientConfiguration {
    @Value("${app.mongo.db-name}")
    @Getter
    @Setter
    private String dbName;

    @Value("${app.mongo.conn-string}")
    @Getter
    @Setter
    private String connString;

    @Override
    protected String getDatabaseName() {
        return getDbName();
    }

    @Override
    public MongoClient mongoClient() {
        ConnectionString connString = new ConnectionString(getConnString());
        MongoClientSettings mongoClientSettings = MongoClientSettings.builder()
                .applyConnectionString(connString)
                .build();

        return MongoClients.create(mongoClientSettings);
    }
}


