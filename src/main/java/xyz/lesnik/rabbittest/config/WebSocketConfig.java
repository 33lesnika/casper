package xyz.lesnik.rabbittest.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import xyz.lesnik.rabbittest.webscoket.SendStatusSocketHandler;
import xyz.lesnik.rabbittest.webscoket.SocketHandler;
import xyz.lesnik.rabbittest.webscoket.StatisticsSocketHandler;

@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

    private final SocketHandler socketHandler;
    private final StatisticsSocketHandler statisticsSocketHandler;
    private final SendStatusSocketHandler sendStatusSocketHandler;

    public WebSocketConfig(SocketHandler socketHandler, StatisticsSocketHandler statisticsSocketHandler, SendStatusSocketHandler sendStatusSocketHandler) {
        this.socketHandler = socketHandler;
        this.statisticsSocketHandler = statisticsSocketHandler;
        this.sendStatusSocketHandler = sendStatusSocketHandler;
    }

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry webSocketHandlerRegistry) {
        webSocketHandlerRegistry.addHandler(socketHandler, "/ws/event").setAllowedOrigins("*");
        webSocketHandlerRegistry.addHandler(sendStatusSocketHandler, "/ws/send-status").setAllowedOrigins("*");
        webSocketHandlerRegistry.addHandler(statisticsSocketHandler, "/ws/stats").setAllowedOrigins("*");
    }
}
