package xyz.lesnik.rabbittest.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.task.TaskExecutorBuilder;
import org.springframework.boot.task.TaskSchedulerBuilder;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.TaskScheduler;
import xyz.lesnik.rabbittest.entity.RecognitionEvent;
import xyz.lesnik.rabbittest.model.RecognitionEventSendStatus;
import xyz.lesnik.rabbittest.repository.RecognitionEventRepository;
import xyz.lesnik.rabbittest.service.BypassArchNameKTSenderProcessor;
import xyz.lesnik.rabbittest.service.FieldCheckingKTSenderProcessor;
import xyz.lesnik.rabbittest.service.KTSenderProcessor;
import xyz.lesnik.rabbittest.webscoket.SendStatusSocketHandler;

import java.util.Collection;
import java.util.Set;
import java.util.function.Consumer;

/**
 * 03.09.2021
 * ApplicationConfig
 * 22:56
 */
@Configuration
public class ApplicationConfig {

    @Autowired
    private RecognitionEventRepository recognitionEventRepository;

    @Autowired
    private AppConfigProperties appConfigProperties;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    private SendStatusSocketHandler sendStatusSocketHandler;

    @Bean
    public TaskScheduler taskScheduler(TaskSchedulerBuilder builder) {
        return builder.poolSize(2).threadNamePrefix("scheduled-task-").build();
    }

    @Bean
    public TaskExecutor applicationExecutor(TaskExecutorBuilder builder) {
        return builder.corePoolSize(2).maxPoolSize(16).build();
    }

    @Bean
    public KTSenderProcessor ktSenderProcessorChain() {
        Consumer<RecognitionEvent> savedCallback = (event) -> {
            recognitionEventRepository.save(event);
            sendStatusSocketHandler.sendAll(new RecognitionEventSendStatus(event.getId().toString(), event.getSendStatus()));
        };
        final FieldCheckingKTSenderProcessor fieldCheckingLink = new FieldCheckingKTSenderProcessor(null, savedCallback, appConfigProperties.getEvents(), applicationEventPublisher);
        return new BypassArchNameKTSenderProcessor(fieldCheckingLink, savedCallback, appConfigProperties.getEvents().getBypassArchIds(), applicationEventPublisher);
    }


}
