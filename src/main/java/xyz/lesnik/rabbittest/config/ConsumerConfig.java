package xyz.lesnik.rabbittest.config;

import lombok.Data;

/**
 * 05.03.2021
 * ConsumerConfig
 * 13:45
 */
@Data
public class ConsumerConfig {
    String exchange;
    String routingKey;
    String queueName;
}
