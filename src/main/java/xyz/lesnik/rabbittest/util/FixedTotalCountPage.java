package xyz.lesnik.rabbittest.util;

import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * 10.02.2022
 * FixedTotalCountPage
 * 14:39
 */
public class FixedTotalCountPage<T> extends PageImpl<T> {

    private final long total;
    private final Pageable pageable;

    public FixedTotalCountPage(List<T> content, Pageable pageable, long total) {
        super(content, pageable, total);
        this.total = total;
        this.pageable = pageable;
    }

    public FixedTotalCountPage(List<T> content) {
        super(content);
        this.total = content.size();
        this.pageable = Pageable.unpaged();
    }

    @Override
    public long getTotalElements() {
        return total;
    }

    @Override
    public int getTotalPages() {
        return getSize() == 0 ? 1 : (int) Math.ceil((double) total / (double) getSize());
    }

    @Override
    public boolean hasNext() {
        return getNumber() < getTotalPages();
    }

    @Override
    public boolean isLast() {
        return !hasNext();
    }

    @Override
    public boolean isFirst() {
        return !pageable.hasPrevious();
    }

    @Override
    public int getNumber() {
        return pageable.isPaged() ? pageable.getPageNumber() : 0;
    }

    @Override
    public int getNumberOfElements() {
        return super.getNumberOfElements();
    }

}
