package xyz.lesnik.rabbittest.webscoket;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;
import xyz.lesnik.rabbittest.entity.SendStatus;
import xyz.lesnik.rabbittest.model.RecognitionEventSendStatusStatistics;
import xyz.lesnik.rabbittest.repository.RecognitionEventRepository;
import xyz.lesnik.rabbittest.service.RecognitionEventService;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * 27.01.2022
 * StatisticsSocketHandler
 * 19:59
 */
@Component
@Slf4j
public class StatisticsSocketHandler extends TextWebSocketHandler {
    private final ObjectMapper objectMapper;
    private final TaskExecutor taskExecutor;
    private final RecognitionEventService recognitionEventService;

    Map<WebSocketSession, StatisticsContext> sessions = new ConcurrentHashMap<>();

    public StatisticsSocketHandler(ObjectMapper objectMapper, TaskExecutor taskExecutor, RecognitionEventService recognitionEventService) {
        this.objectMapper = objectMapper;
        this.taskExecutor = taskExecutor;
        this.recognitionEventService = recognitionEventService;
    }

    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message)
            throws InterruptedException, IOException {
        try {
            final StatisticsContext newContext = objectMapper.readValue(message.getPayload(), StatisticsContext.class);
            sessions.put(session, newContext);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        final StatisticsContext context = new StatisticsContext();
        sessions.put(session, context);
        session.sendMessage(getStatsMessage());
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        sessions.remove(session);
        super.afterConnectionClosed(session, status);
    }

    public void sendAll(Object o) {
        taskExecutor.execute(() -> sessions.forEach((s, v) -> {
            try {
                s.sendMessage(new TextMessage(objectMapper.writeValueAsString(o)));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }));
    }

    public void sendAll(WebSocketMessage<?> msg) {
        taskExecutor.execute(() -> sessions.forEach((s, v) -> {
            try {
                s.sendMessage(msg);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }));
    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
        super.handleTransportError(session, exception);
        exception.printStackTrace();
    }

    @Scheduled(fixedDelayString = "PT30S")
    public void sendStats() {
        taskExecutor.execute(() -> {
            TextMessage statsMessage = getStatsMessage();
            sendAll(statsMessage);
        });
    }

    private TextMessage getStatsMessage() {
        final List<RecognitionEventSendStatusStatistics> message = recognitionEventService.getStatistics().stream().map(stats -> {
            RecognitionEventSendStatusStatistics model = new RecognitionEventSendStatusStatistics();
            model.setStatus(stats.getSendStatus());
            model.setCount(stats.getCount());
            return model;
        }).collect(Collectors.toList());
        try {
            return new TextMessage(objectMapper.writeValueAsString(message));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
