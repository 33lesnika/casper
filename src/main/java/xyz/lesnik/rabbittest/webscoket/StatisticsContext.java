package xyz.lesnik.rabbittest.webscoket;

import lombok.Data;

/**
 * 27.01.2022
 * StatisticsContext
 * 20:05
 */
@Data
public class StatisticsContext {
    int page = 0;
    int size = 10;
}
