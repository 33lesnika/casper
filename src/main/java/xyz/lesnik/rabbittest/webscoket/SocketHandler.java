package xyz.lesnik.rabbittest.webscoket;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@Component
@Slf4j
public class SocketHandler extends TextWebSocketHandler {

    private final ObjectMapper objectMapper;
    private final TaskExecutor taskExecutor;

    List<WebSocketSession> sessions = new CopyOnWriteArrayList<>();

    public SocketHandler(ObjectMapper objectMapper, TaskExecutor taskExecutor) {
        this.objectMapper = objectMapper;
        this.taskExecutor = taskExecutor;
    }

    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message)
            throws InterruptedException, IOException {

//        for(WebSocketSession webSocketSession : sessions) {
//            webSocketSession.sendMessage(new TextMessage("Hello " + value.get("name") + " !"));
//        }
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        sessions.add(session);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        sessions.remove(session);
        super.afterConnectionClosed(session, status);
    }

    public void sendAll(Object o) {
        taskExecutor.execute(() -> {
            sessions.forEach(s -> {
                try {
                    s.sendMessage(new TextMessage(objectMapper.writeValueAsString(o)));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        });
    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
        super.handleTransportError(session, exception);
        exception.printStackTrace();
    }
}
