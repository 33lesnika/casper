package xyz.lesnik.rabbittest.service;

import org.springframework.stereotype.Service;
import xyz.lesnik.rabbittest.entity.RecognitionEvent;
import xyz.lesnik.rabbittest.entity.Violation;
import xyz.lesnik.rabbittest.service.violation.ViolationDetector;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 19.12.2020
 * ViolationServiceImpl
 * 20:44
 */
@Service
public class ViolationServiceImpl implements ViolationService {

    private final Set<ViolationDetector> violationDetectors;

    public ViolationServiceImpl(Set<ViolationDetector> violationDetectors) {
        this.violationDetectors = violationDetectors;
    }

    @Override
    public Set<Violation> checkViolations(RecognitionEvent recognitionEvent) {
        return violationDetectors.stream()
                .map(vd -> vd.detectViolation(recognitionEvent))
                .flatMap(Optional::stream)
                .collect(Collectors.toSet());
    }
}
