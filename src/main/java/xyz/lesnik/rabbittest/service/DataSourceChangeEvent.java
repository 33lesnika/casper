package xyz.lesnik.rabbittest.service;

import org.springframework.context.ApplicationEvent;
import xyz.lesnik.rabbittest.entity.DataSourceSettings;

/**
 * 31.03.2021
 * DataSourceChangeEvent
 * 14:49
 */
public class DataSourceChangeEvent extends ApplicationEvent {
    private DataSourceSettings settings;

    public DataSourceChangeEvent(Object source, DataSourceSettings settings) {
        super(source);
        this.settings = settings;
    }
    public DataSourceSettings getDataSourceSettings() {
        return settings;
    }
}
