package xyz.lesnik.rabbittest.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.util.StringUtils;
import xyz.lesnik.rabbittest.config.EventConfig;
import xyz.lesnik.rabbittest.entity.RecognitionEvent;
import xyz.lesnik.rabbittest.event.SendToVshepEvent;

import java.util.function.Consumer;

/**
 * 04.02.2022
 * FieldCheckingKTSenderProcessor
 * 23:12
 */
@Slf4j
public class FieldCheckingKTSenderProcessor extends KTSenderProcessor {

    private final EventConfig eventConfig;
    private final ApplicationEventPublisher publisher;
    private final String minPhotoMessage;
    private final String minWeightMessage;
    private final String minSpeedMessage;

    public FieldCheckingKTSenderProcessor(KTSenderProcessor next, Consumer<RecognitionEvent> afterProcessCallback, EventConfig eventConfig, ApplicationEventPublisher publisher) {
        super(next, afterProcessCallback);
        this.eventConfig = eventConfig;
        this.publisher = publisher;
        this.minPhotoMessage = "Не хватает фотографий для отправки в КТ, нужно минимум: " + eventConfig.getPhotosMin();
        this.minWeightMessage = "Вес меньше минимального, минимум: " + eventConfig.getWeightMin();
        this.minSpeedMessage = "Скорость меньше минимальной, минимум: " + eventConfig.getSpeedMin();
    }

    @Override
    public void process(RecognitionEvent recognitionEvent) {
        if (!validateFields(recognitionEvent)) {
            log.info("Event {} {} is invalid, not sending to KT, reasons: {}", recognitionEvent.getGrnz(), recognitionEvent.getTimestamp(), String.join(";", recognitionEvent.getNotSentReasons()));
            next(recognitionEvent);
            return;
        }
        publisher.publishEvent(new SendToVshepEvent(this, recognitionEvent, true, getAfterProcessCallback()));
    }

    private boolean validateFields(RecognitionEvent recognitionEvent) {
        boolean valid = true;
        if (recognitionEvent.getGrossWeight() < eventConfig.getWeightMin()) {
            recognitionEvent.getNotSentReasons().add(minWeightMessage);
            valid = false;
        }
        if (!StringUtils.hasText(recognitionEvent.getGrnz())) {
            recognitionEvent.getNotSentReasons().add("Нет ГРНЗ или он пуст");
            valid = false;
        }
        if (StringUtils.hasText(recognitionEvent.getGrnz()) && recognitionEvent.getGrnz().contains("NOPLATE")) {
            recognitionEvent.getNotSentReasons().add("ГРНЗ содержит 'NOPLATE'");
            valid = false;
        }
        if (recognitionEvent.getVelocity() <= eventConfig.getSpeedMin()) {
            recognitionEvent.getNotSentReasons().add(minSpeedMessage);
            valid = false;
        }
        if (photosCount(recognitionEvent) < eventConfig.getPhotosMin()) {
            recognitionEvent.getNotSentReasons().add(minPhotoMessage);
            valid = false;
        }
        return valid;
    }

    private static int photosCount(RecognitionEvent recognitionEvent) {
        int count = 0;
        if (recognitionEvent.getAnprfrontId() != null) {
            count++;
        }
        if (recognitionEvent.getAnprrearId() != null) {
            count++;
        }
        if (recognitionEvent.getOvcfrontId() != null) {
            count++;
        }
        if (recognitionEvent.getPlatefrontId() != null) {
            count++;
        }
        if (recognitionEvent.getPlaterearId() != null) {
            count++;
        }
        return count;
    }
}
