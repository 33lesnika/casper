package xyz.lesnik.rabbittest.service;

import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import xyz.lesnik.rabbittest.config.AppConfigProperties;
import xyz.lesnik.rabbittest.entity.RecognitionEvent;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Collection;

@Service
@Slf4j
public class InvalidEventStorage {

    private final ObjectMapper objectMapper;
    private final AppConfigProperties appConfigProperties;

    public InvalidEventStorage(ObjectMapper objectMapper, AppConfigProperties appConfigProperties) {
        this.objectMapper = objectMapper;
        this.appConfigProperties = appConfigProperties;
    }

    public RecognitionEvent save(RecognitionEvent recognitionEvent) {
        ObjectWriter writer = objectMapper.writer(new DefaultPrettyPrinter());
        try {
            writer.writeValue(Path.of(appConfigProperties.getEvents().getInvalidFilesPath(),
                    System.currentTimeMillis() + ".json").toFile(), recognitionEvent);
        } catch (IOException e) {
            log.warn("Cannot save invalid event: {}", e.getMessage());
        }
        return recognitionEvent;
    }
}
