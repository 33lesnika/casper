package xyz.lesnik.rabbittest.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import xyz.lesnik.rabbittest.config.AppConfigProperties;
import xyz.lesnik.rabbittest.controller.dto.CarTypeResponse;
import xyz.lesnik.rabbittest.controller.dto.EventFilterResponse;
import xyz.lesnik.rabbittest.controller.dto.FilterParams;
import xyz.lesnik.rabbittest.controller.dto.ViolationTypeResponse;
import xyz.lesnik.rabbittest.entity.*;
import xyz.lesnik.rabbittest.event.SendToVshepEvent;
import xyz.lesnik.rabbittest.model.SendStatusCount;
import xyz.lesnik.rabbittest.repository.RecognitionEventRepository;
import xyz.lesnik.rabbittest.repository.RecognitonEventFilterRepository;
import xyz.lesnik.rabbittest.security.authentication.UserAccount;
import xyz.lesnik.rabbittest.webscoket.SocketHandler;

import javax.transaction.Transactional;
import java.util.*;

/**
 * 14.12.2020
 * RecognitionEventServiceImpl
 * 21:45
 */

@Service
@Slf4j
public class RecognitionEventServiceImpl implements RecognitionEventService {
    private final RecognitionEventRepository recognitionEventRepository;
    private final ViolationService violationService;
    private final SocketHandler socketHandler;
    private final CarTypeService carTypeService;
    private final DataSourceSettingsService dataSourceSettingsService;
    private final RecognitonEventFilterRepository recognitonEventFilterRepository;
    private final ApplicationEventPublisher publisher;
    private final RecognitionEventHelper helper;
    private final JdbcTemplate jdbcTemplate;
    private final AppConfigProperties appConfigProperties;
    private final KTSenderProcessor ktSenderProcessor;
    private final InvalidEventStorage invalidEventStorage;

    public RecognitionEventServiceImpl(RecognitionEventRepository recognitionEventRepository, ViolationService violationService, SocketHandler socketHandler, CarTypeService carTypeService, DataSourceSettingsService dataSourceSettingsService, RecognitonEventFilterRepository recognitonEventFilterRepository, ApplicationEventPublisher publisher, RecognitionEventHelper helper, JdbcTemplate jdbcTemplate, AppConfigProperties appConfigProperties, KTSenderProcessor ktSenderProcessor, InvalidEventStorage invalidEventStorage) {
        this.recognitionEventRepository = recognitionEventRepository;
        this.violationService = violationService;
        this.socketHandler = socketHandler;
        this.carTypeService = carTypeService;
        this.dataSourceSettingsService = dataSourceSettingsService;
        this.recognitonEventFilterRepository = recognitonEventFilterRepository;
        this.publisher = publisher;
        this.helper = helper;
        this.jdbcTemplate = jdbcTemplate;
        this.appConfigProperties = appConfigProperties;
        this.ktSenderProcessor = ktSenderProcessor;
        this.invalidEventStorage = invalidEventStorage;
    }

    @Override
    public RecognitionEvent create(RecognitionEvent recognitionEvent) {
        if (StringUtils.hasText(recognitionEvent.getGrnz()) && recognitionEvent.getGrnz().length() > 15) {
            log.warn("Size of GRNZ exceeds limit of database column: {}", recognitionEvent.getGrnz());
            invalidEventStorage.save(recognitionEvent);
            return recognitionEvent;
        }
//        log.info(recognitionEvent.toString());
        if (recognitionEvent.getId() != null) {
            var byId = recognitionEventRepository.findById(recognitionEvent.getId());
            if (byId.isPresent()) {
                recognitionEvent = byId.get();
            }
        }

        Set<Violation> violations = violationService.checkViolations(recognitionEvent);
        if (!violations.isEmpty()) {
            recognitionEvent.setViolations(violations);
        }

        if (recognitionEvent.getCarType() == null) {
            CarType carType = carTypeService.resolveCarType(recognitionEvent.getAxlesCount(), recognitionEvent.getClassWIM());
            if (carType != null) {
                recognitionEvent.setCarType(carType);
            } else {
                if (recognitionEvent.getViolations() == null) {
                    recognitionEvent.setViolations(new HashSet<>());
                }
                recognitionEvent.getViolations().add(
                        new Violation(ViolationType.TYPE_NOT_FOUND,
                                "Тип ТС не определён",
                                "Нет данных по типу ТС с количестом осей "
                                        + recognitionEvent.getAxlesCount()
                                        + ", класс "
                                        + recognitionEvent.getClassWIM(),
                                0d));
            }
        }

        ktSenderProcessor.process(recognitionEvent);
        var saved = recognitionEventRepository.save(recognitionEvent);
        helper.updateArchNames(saved);
        socketHandler.sendAll(saved);
        return saved;
    }


    @Override
    public RecognitionEvent get(UUID uuid) {
        return recognitionEventRepository.findById(uuid).orElseThrow(RuntimeException::new);
    }

    @Override
    public RecognitionEvent send(UUID uuid) {
        final RecognitionEvent event = recognitionEventRepository.findById(uuid).orElseThrow(RuntimeException::new);
        event.setSendStatus(SendStatus.SENDING);
        if (appConfigProperties.getEvents().getBypassArchIds().contains(event.getName())) {
            publisher.publishEvent(new SendToVshepEvent(this, event, false, recognitionEventRepository::save));
        } else {
            publisher.publishEvent(new SendToVshepEvent(this, event, true, recognitionEventRepository::save));
        }
        return event;
    }

    @Override
    public Page<RecognitionEvent> getAll() {
        return recognitionEventRepository.findAllByOrderByTimestampDesc(PageRequest.of(0, 100));
//        return recognitionEventRepository.findAllByViolationsIsNotNullOrderByTimestampDesc(PageRequest.of(0, 100));
    }

    @Override
    public List<RecognitionEvent> getAllBySendStatus(SendStatus sendStatus) {
        return recognitionEventRepository.findAllBySendStatus(sendStatus);
//        return recognitionEventRepository.findAllByViolationsIsNotNullOrderByTimestampDesc(PageRequest.of(0, 100));
    }

    @Override
    public RecognitionEvent update(RecognitionEvent recognitionEvent) {
        return recognitionEventRepository.save(recognitionEvent);
    }

    @Override
    public void delete(UUID uuid) {
        recognitionEventRepository.deleteById(uuid);
    }

    @Override
    public Page<RecognitionEvent> findBetween(Long from, Long to, Pageable pageable) {
        return recognitionEventRepository.findAllByTimestampBetween(from, to, pageable);
    }

    @Override
    public RecognitionEvent changeCarType(UUID id, CarType carType) {
        RecognitionEvent event = get(id);
        event.setCarType(carType);
        event.setAxlesCount(carType.getAxles());
        if (event.getHistory() == null) {
            event.setHistory(new ArrayList<>());
        }
        event.getHistory().add(History.builder()
                .username(getCurrentUser().getUsername())
                .action("Изменён тип ТС: " + carType.toString())
                .timestamp(System.currentTimeMillis())
                .build());
        event.setViolations(null);
        return create(event);
    }

    private UserAccount getCurrentUser() {
        return (UserAccount) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    @Override
    public EventFilterResponse getFilterData() {
        EventFilterResponse result = new EventFilterResponse();
        for (ViolationType violation : ViolationType.values()) {
            result.getViolationList().add(new ViolationTypeResponse(violation.name(), violation.getDescription()));
        }
        carTypeService.getAll().stream()
                .map(x -> new CarTypeResponse(x.getId(), x.getName()))
                .forEach(result.getCarTypeList()::add);
        dataSourceSettingsService.getAll().stream()
                .map(DataSourceSettings::getAlias)
                .forEach(result.getAliasList()::add);
        return result;
    }

    public Page<RecognitionEvent> getEvents(FilterParams params, Pageable pageable) {
        if (pageable == null) {
            pageable = PageRequest.of(0, 50);
        }
        return recognitonEventFilterRepository.getEvents(params, pageable);
    }

    public List<SendStatusCount> getStatistics() {
        String sql = "select send_status as send_status, count(*) as count from recognition_event group by send_status";
        final List<SendStatusCount> stats = jdbcTemplate.query(sql, (rs, row) -> {
            SendStatusCount count = new SendStatusCount();
            String status = rs.getString(1);
            if (!StringUtils.hasText(status)) {
                count.setSendStatus(SendStatus.UNKNOWN);
            } else {
                count.setSendStatus(SendStatus.valueOf(status));
            }
            count.setCount(rs.getLong(2));
            return count;
        });
        return stats;
    }

    @Override
    @Transactional
    public RecognitionEvent correctGrnz(UUID uuid, String correctedGrnz) {
        return correctGrnz(uuid, correctedGrnz, getCurrentUser().getUsername());
    }

    private RecognitionEvent correctGrnz(UUID uuid, String correctedGrnz, String username) {
        RecognitionEvent event = get(uuid);
        event.setCorrectedGrnz(correctedGrnz);
        event.setModifiedBy(username);
        event.setModificationTimestamp(System.currentTimeMillis());
        return recognitionEventRepository.save(event);
    }
}
