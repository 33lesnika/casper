package xyz.lesnik.rabbittest.service;

import xyz.lesnik.rabbittest.entity.RecognitionEvent;

import java.util.Optional;
import java.util.function.Consumer;

/**
 * 04.02.2022
 * KTSenderProcessor
 * 22:08
 */
public abstract class KTSenderProcessor {

    private final Optional<KTSenderProcessor> next;
    private final Consumer<RecognitionEvent> afterProcessCallback;

    public KTSenderProcessor(KTSenderProcessor next, Consumer<RecognitionEvent> afterProcessCallback) {
        this.next = Optional.ofNullable(next);
        this.afterProcessCallback = afterProcessCallback;
    }

    public abstract void process(RecognitionEvent recognitionEvent);

    protected void next(RecognitionEvent recognitionEvent) {
        next.ifPresent(p -> p.process(recognitionEvent));
    }

    protected Consumer<RecognitionEvent> getAfterProcessCallback() {
        return afterProcessCallback;
    }
}
