package xyz.lesnik.rabbittest.service;

import org.springframework.stereotype.Service;
import xyz.lesnik.rabbittest.entity.CarType;
import xyz.lesnik.rabbittest.repository.CarTypeRepository;

import java.util.List;

/**
 * 28.01.2021
 * CarTypeService
 * 12:21
 */
@Service
public class CarTypeService {

    private final CarTypeRepository carTypeRepository;

    public CarTypeService(CarTypeRepository carTypeRepository) {
        this.carTypeRepository = carTypeRepository;
    }

    public List<CarType> getAll() {
        return carTypeRepository.findAll();
    }

    public Iterable<CarType> saveAll(List<CarType> carTypeList) {
        return carTypeRepository.saveAll(carTypeList);
    }

    public CarType resolveCarType(Integer axlesCount, Integer classWIM) {
        return getAll()
                .stream()
                .filter(carType -> carType.getAxles() == axlesCount && carType.getType() == classWIM)
                .findFirst()
                .orElseGet(() -> getCarTypeByAxlesCount(axlesCount));
    }

    //TODO: узнать у заказчика, что делать если не распознано сочетание тип/оси
    private CarType getCarTypeByAxlesCount(Integer axlesCount) {
        return null;
    }
}
