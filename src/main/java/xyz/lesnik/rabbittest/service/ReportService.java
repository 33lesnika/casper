package xyz.lesnik.rabbittest.service;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.stereotype.Service;
import xyz.lesnik.rabbittest.controller.dto.*;
import xyz.lesnik.rabbittest.entity.RecognitionEvent;
import xyz.lesnik.rabbittest.entity.Violation;
import xyz.lesnik.rabbittest.repository.RecognitionEventRepository;
import xyz.lesnik.rabbittest.repository.RecognitonEventFilterRepository;

import javax.transaction.Transactional;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * 30.01.2021
 * ReportService
 * 11:57
 */
@Service
public class ReportService {

    private final RecognitionEventRepository recognitionEventRepository;
    private final RecognitonEventFilterRepository recognitonEventFilterRepository;

    public ReportService(RecognitionEventRepository recognitionEventRepository, RecognitonEventFilterRepository recognitonEventFilterRepository) {
        this.recognitionEventRepository = recognitionEventRepository;
        this.recognitonEventFilterRepository = recognitonEventFilterRepository;
    }

    private CellStyle dateStyle;

    @Transactional
    public void writeContent(Long from, Long to, OutputStream outputStream) throws IOException {
        SXSSFWorkbook book = new SXSSFWorkbook();
        SXSSFSheet sheet = book.createSheet("Все события");
        sheet.trackAllColumnsForAutoSizing();

        DataFormat format = book.createDataFormat();
        dateStyle = book.createCellStyle();
        dateStyle.setDataFormat(format.getFormat("yyyy/MM/dd HH:mm:ss"));

        writeHeader(sheet.createRow(0));
        AtomicInteger rowNum = new AtomicInteger(1);
        recognitionEventRepository.findAllByTimestampBetween(from, to)
                .forEach(re -> {
                    SXSSFRow row = sheet.createRow(rowNum.getAndIncrement());
                    writeCells(row, re);
                });
        sheet.flushRows();
        for (int i = 0; i < 9; i++) {
            sheet.autoSizeColumn(i);
        }

        book.write(outputStream);
        book.close();
    }

    @Transactional
    public void writeContent(FilterParams filterParams, OutputStream outputStream) throws IOException {
        SXSSFWorkbook book = new SXSSFWorkbook();
        SXSSFSheet sheet = book.createSheet("Все события");
        sheet.trackAllColumnsForAutoSizing();

        DataFormat format = book.createDataFormat();
        dateStyle = book.createCellStyle();
        dateStyle.setDataFormat(format.getFormat("yyyy/MM/dd HH:mm:ss"));

        writeHeader(sheet.createRow(0));
        AtomicInteger rowNum = new AtomicInteger(1);
        recognitonEventFilterRepository.getEvents(filterParams)
                .forEach(re -> {
                    SXSSFRow row = sheet.createRow(rowNum.getAndIncrement());
                    writeCells(row, re);
                });
        sheet.flushRows();
        for (int i = 0; i < 9; i++) {
            sheet.autoSizeColumn(i);
        }

        book.write(outputStream);
        book.close();
    }

    private void writeHeader(SXSSFRow row) {
        row.createCell(0).setCellValue("Время");
        row.createCell(1).setCellValue("Номер");
        row.createCell(2).setCellValue("Направление");
        row.createCell(3).setCellValue("Вес");
        row.createCell(4).setCellValue("Длина");
        row.createCell(5).setCellValue("Ширина");
        row.createCell(6).setCellValue("Высота");
        row.createCell(7).setCellValue("Количество осей");
        row.createCell(8).setCellValue("Нарушения");
    }

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

    private void writeCells(Row row, RecognitionEvent re) {
        var dateTimeCell = row.createCell(0);
        dateTimeCell.setCellStyle(dateStyle);
        dateTimeCell.setCellValue(dateFormat.format(new Date(re.getTimestamp())));
        row.createCell(1).setCellValue(re.getGrnz());
        row.createCell(2).setCellValue(re.getDirection());
        row.createCell(3).setCellValue(re.getGrossWeight());
        row.createCell(4).setCellValue(re.getVehicleLength());
        row.createCell(5).setCellValue(re.getVehicleWidth());
        row.createCell(6).setCellValue(re.getVehicleHeight());
        row.createCell(7).setCellValue(re.getAxlesCount());
        row.createCell(8).setCellValue(re.getViolations() != null ? re.getViolations().stream().map(Violation::getDetails).collect(Collectors.joining(",")) : "");
    }

    public SummaryReportResponse generateSummaryReport(SummaryReportRequest summaryReportRequest) {
        final long start = summaryReportRequest.getFrom();
        final long end = summaryReportRequest.getTo();
        final List<SummaryCountsInterface> summaryCounts = recognitionEventRepository.generateStatistics(start, end);
        SummaryReportResponse response = new SummaryReportResponse();
        response.setArchCounts(summaryCounts);
        response.setFrom(start);
        response.setTo(end);
        response.setTotalCounts(summaryCounts.stream().map(ReportService::toSummaryCount).reduce(new SummaryCounts(), ReportService::merge));
        return response;
    }

    private static SummaryCounts toSummaryCount(SummaryCountsInterface sc) {
        return new SummaryCounts(sc.getArch(), sc.getOverweightCount(), sc.getNoViolationsCount(), sc.getAllCount(), sc.getSentCount());
    }

    private static SummaryCounts merge(SummaryCounts subtotal, SummaryCounts element) {
        subtotal.setSentCount(subtotal.getSentCount() + element.getSentCount());
        subtotal.setOverweightCount(subtotal.getOverweightCount() + element.getOverweightCount());
        subtotal.setNoViolationsCount(subtotal.getNoViolationsCount() + element.getNoViolationsCount());
        subtotal.setAllCount(subtotal.getAllCount() + element.getAllCount());
        subtotal.setArch(null);
        return subtotal;
    }

    public PercentageReportResponse generatePercentageReport(SummaryReportRequest summaryReportRequest) {
        final long start = summaryReportRequest.getFrom();
        final long end = summaryReportRequest.getTo();
        final PercentageInfo percentageInfo = recognitionEventRepository.generatePercentageReport(start, end);
        PercentageReportResponse response = new PercentageReportResponse();
        response.setFrom(start);
        response.setTo(end);
        double normalPercentage = BigDecimal.valueOf(((double) percentageInfo.getNoViolationsCount()) / percentageInfo.getAllCount())
                .setScale(4, RoundingMode.HALF_UP).doubleValue() * 100;
        response.setNormalPercentage(normalPercentage);
        response.setViolationPercentage(100 - normalPercentage);
        return response;
    }
}
