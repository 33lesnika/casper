package xyz.lesnik.rabbittest.service;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import xyz.lesnik.rabbittest.RabbitConnectionHolder;
import xyz.lesnik.rabbittest.entity.DataSourceSettings;
import xyz.lesnik.rabbittest.exception.BadRequestException;
import xyz.lesnik.rabbittest.exception.NotFoundException;
import xyz.lesnik.rabbittest.repository.DataSourceSettingsRepository;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * 30.03.2021
 * DataSourceSettingsService
 * 08:53
 */
@Service
public class DataSourceSettingsService {
    private final DataSourceSettingsRepository repository;
    private final RabbitConnectionHolder rabbitConnectionHolder;
    private final ApplicationEventPublisher publisher;

    public DataSourceSettingsService(DataSourceSettingsRepository repository, RabbitConnectionHolder rabbitConnectionHolder, ApplicationEventPublisher publisher) {
        this.repository = repository;
        this.rabbitConnectionHolder = rabbitConnectionHolder;
        this.publisher = publisher;
    }

    @Transactional
    public Collection<DataSourceSettings> getAll() {
        return repository.findAllBy()
                .peek(ds -> ds.setStatus(rabbitConnectionHolder.isConnectionAlive(ds) ? "Работает" : "Не работает"))
                .collect(Collectors.toList());
    }

    @Transactional
    public DataSourceSettings create(DataSourceSettings settings) {
        validate(settings);
        var saved = repository.save(settings);
        publisher.publishEvent(new DataSourceChangeEvent(this, settings));
        saved.setStatus(rabbitConnectionHolder.isConnectionAlive(saved) ? "Работает" : "Не работает");
        return saved;
    }

    @Transactional
    public DataSourceSettings update(Long id, DataSourceSettings settings) {
        var saved = repository.findById(id).orElseThrow(() -> new NotFoundException("Объект с таким ID не найден: " + id));
        settings.setId(id);
        if (!StringUtils.hasText(settings.getPassword())) {
            settings.setPassword(saved.getPassword());
        }
        var updated = repository.save(settings);
        publisher.publishEvent(new DataSourceChangeEvent(this, settings));
        updated.setStatus(rabbitConnectionHolder.isConnectionAlive(saved) ? "Работает" : "Не работает");
        return updated;
    }

    @Transactional
    public void delete(Long id) {
        repository.deleteById(id);
        publisher.publishEvent(new DataSourceChangeEvent(this, null));
    }

    private void validate(DataSourceSettings settings) {
        if (!StringUtils.hasText(settings.getHost())) {
            throw new BadRequestException("поле 'host' не может быть пустым!");
        }
        if (!StringUtils.hasText(settings.getUsername())) {
            throw new BadRequestException("поле 'username' не может быть пустым!");
        }
        if (!StringUtils.hasText(settings.getPassword())) {
            throw new BadRequestException("поле 'password' не может быть пустым!");
        }
        if (!StringUtils.hasText(settings.getQueue())) {
            throw new BadRequestException("поле 'queue' не может быть пустым!");
        }
    }
}
