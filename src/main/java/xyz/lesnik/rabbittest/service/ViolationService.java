package xyz.lesnik.rabbittest.service;

import xyz.lesnik.rabbittest.entity.RecognitionEvent;
import xyz.lesnik.rabbittest.entity.Violation;

import java.util.Set;

/**
 * 19.12.2020
 * ViolationService
 * 20:44
 */
public interface ViolationService {
    Set<Violation> checkViolations(RecognitionEvent recognitionEvent);
}
