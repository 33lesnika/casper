package xyz.lesnik.rabbittest.service;

import org.springframework.stereotype.Service;
import xyz.lesnik.rabbittest.controller.dto.ArchCodePercentage;
import xyz.lesnik.rabbittest.controller.dto.ArchStatistics;
import xyz.lesnik.rabbittest.controller.dto.PercentageInfo;
import xyz.lesnik.rabbittest.entity.Arch;
import xyz.lesnik.rabbittest.repository.ArchRepository;
import xyz.lesnik.rabbittest.repository.RecognitionEventRepository;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 31.01.2022
 * ArchService
 * 21:07
 */
@Service
public class ArchService {

    private final ArchRepository archRepository;
    private final RecognitionEventRepository recognitionEventRepository;

    public ArchService(ArchRepository archRepository, RecognitionEventRepository recognitionEventRepository) {
        this.archRepository = archRepository;
        this.recognitionEventRepository = recognitionEventRepository;
    }

    public List<Arch> getArchData(long from, long to) {
        if (to <= 0) to = Long.MAX_VALUE;
        final List<Arch> arches = archRepository.findAll();
        final Map<String, ArchStatistics> archStatistics = recognitionEventRepository
                .generateViolationsReportByArchCode(from, to)
                .stream()
                .collect(Collectors.toMap(ArchStatistics::getName, Function.identity()));
        arches.forEach(arch -> {
            AtomicLong allCount = new AtomicLong(0L);
            AtomicLong noViolationsCount = new AtomicLong(0L);
            arch.setArchCodePercentages(arch.getArchCodes().stream()
                    .map(archStatistics::get)
                    .filter(Objects::nonNull)
                    .map(this::mapArchCodePercentage)
                    .collect(Collectors.toList()));
            final List<ArchStatistics> archStats = arch.getArchCodes().stream()
                    .map(archStatistics::get)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
            archStats.forEach(archStat -> {
                allCount.addAndGet(archStat.getAllCount());
                noViolationsCount.addAndGet(archStat.getNoViolationsCount());
            });
            arch.setNoViolationsPercentage(noViolationsCount.get() / ((double) allCount.get()));
            arch.setViolationsPercentage(1.0 - arch.getNoViolationsPercentage());
        });
        return arches;
    }

    private ArchCodePercentage mapArchCodePercentage(ArchStatistics archStatistics) {
        ArchCodePercentage percentage = new ArchCodePercentage();
        percentage.setCode(archStatistics.getName());
        final double allCount = archStatistics.getAllCount();
        percentage.setNoViolationPercentage(archStatistics.getNoViolationsCount() / allCount);
        percentage.setViolationPercentage(1 - percentage.getNoViolationPercentage());
        return percentage;
    }
}
