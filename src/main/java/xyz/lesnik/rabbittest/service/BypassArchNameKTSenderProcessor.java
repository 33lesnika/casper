package xyz.lesnik.rabbittest.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import xyz.lesnik.rabbittest.entity.RecognitionEvent;
import xyz.lesnik.rabbittest.event.SendToVshepEvent;

import java.util.Collection;
import java.util.function.Consumer;

/**
 * 04.02.2022
 * BypassArchNameKTSenderProcessor
 * 22:51
 */
@Slf4j
public class BypassArchNameKTSenderProcessor extends KTSenderProcessor {

    private final Collection<String> bypassArchIds;
    private final ApplicationEventPublisher publisher;

    public BypassArchNameKTSenderProcessor(KTSenderProcessor next, Consumer<RecognitionEvent> afterProcessCallback, Collection<String> bypassArchIds, ApplicationEventPublisher publisher) {
        super(next, afterProcessCallback);
        this.publisher = publisher;
        this.bypassArchIds = bypassArchIds;
    }

    @Override
    public void process(RecognitionEvent recognitionEvent) {
        boolean bypassCharacters = bypassArchIds.contains(recognitionEvent.getName());
        if (bypassCharacters) {
            log.info("Sending event with uncut GRNZ");
            publisher.publishEvent(new SendToVshepEvent(this, recognitionEvent, false, getAfterProcessCallback()));
            return;
        }
        next(recognitionEvent);
    }
}
