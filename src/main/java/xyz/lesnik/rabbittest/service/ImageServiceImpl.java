package xyz.lesnik.rabbittest.service;

import lombok.Setter;
import org.springframework.stereotype.Service;
import xyz.lesnik.rabbittest.entity.Image;
import xyz.lesnik.rabbittest.exception.NotFoundException;
import xyz.lesnik.rabbittest.repository.ImageRepository;

import java.util.UUID;

/**
 * 14.12.2020
 * ImageServiceImpl
 * 22:41
 */

@Service
public class ImageServiceImpl implements ImageService {
    private final ImageRepository imageRepository;

    public ImageServiceImpl(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }

    @Override
    public Image create(Image image) {
        return imageRepository.create(image);
    }

    @Override
    public Image get(UUID uuid) {
        return imageRepository.get(uuid).orElseThrow(NotFoundException::new);
    }

    @Override
    public Iterable<Image> getAll() {
        return imageRepository.getAll();
    }

    @Override
    public void delete(Image image) {
        imageRepository.delete(image);
    }
}
