package xyz.lesnik.rabbittest.service;

/**
 * 31.01.2022
 * CoordinateService
 * 20:54
 */
public class CoordinateService {
    final static double RADIUS_MAJOR = 6378137.0;
    final static double RADIUS_MINOR = 6356752.3142;
//    Height: 2497579.320624926
//    Width: 4552054.353620384
    final static double KAZAKHSTAN_HEIGHT = 2497579.320624926;
    final static double KAZAKHSTAN_WIDTH = 4552054.353620384;
    final static double KAZAKHSTAN_WEST = 5153391.110936569;
    final static double KAZAKHSTAN_EAST = 9705445.464556953;
    final static double KAZAKHSTAN_NORTH = 7413155.417402594;
    final static double KAZAKHSTAN_SOUTH = 4915576.096777668;
    final static double STRETCH_RECTANGLE_HEIGHT = 100.0;
    final static double STRETCH_RECTANGLE_WIDTH = 100.0;

    public static double yStretched(double input){
        double projected = yAxisMercator(input) - KAZAKHSTAN_SOUTH;
        double mapped = projected * STRETCH_RECTANGLE_HEIGHT / KAZAKHSTAN_HEIGHT;
        return STRETCH_RECTANGLE_HEIGHT - mapped;
    }

    public static double xStretched(double input){
        double projected = xAxisMercator(input) - KAZAKHSTAN_WEST;
        return projected * STRETCH_RECTANGLE_WIDTH / KAZAKHSTAN_WIDTH;
    }

    public static double yAxisMercator(double input) {

        input = Math.min(Math.max(input, -89.5), 89.5);
        double earthDimensionalRateNormalized = 1.0 - Math.pow(RADIUS_MINOR / RADIUS_MAJOR, 2);

        double inputOnEarthProj = Math.sqrt(earthDimensionalRateNormalized) *
                Math.sin( Math.toRadians(input));

        inputOnEarthProj = Math.pow(((1.0 - inputOnEarthProj) / (1.0+inputOnEarthProj)),
                0.5 * Math.sqrt(earthDimensionalRateNormalized));

        double inputOnEarthProjNormalized =
                Math.tan(0.5 * ((Math.PI * 0.5) - Math.toRadians(input))) / inputOnEarthProj;

        return (-1) * RADIUS_MAJOR * Math.log(inputOnEarthProjNormalized);
    }

    public static double xAxisMercator(double input) {
        return RADIUS_MAJOR * Math.toRadians(input);
    }
}
