package xyz.lesnik.rabbittest.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import xyz.lesnik.rabbittest.controller.dto.EventFilterResponse;
import xyz.lesnik.rabbittest.controller.dto.FilterParams;
import xyz.lesnik.rabbittest.entity.CarType;
import xyz.lesnik.rabbittest.entity.RecognitionEvent;
import xyz.lesnik.rabbittest.entity.SendStatus;
import xyz.lesnik.rabbittest.model.SendStatusCount;

import java.util.List;
import java.util.UUID;

/**
 * 14.12.2020
 * RecognitionEventService
 * 21:42
 */
public interface RecognitionEventService {
    RecognitionEvent create(RecognitionEvent recognitionEvent);
    RecognitionEvent get(UUID uuid);
    RecognitionEvent send(UUID uuid);
    Page<RecognitionEvent> getAll();

    List<RecognitionEvent> getAllBySendStatus(SendStatus sendStatus);

    RecognitionEvent update(RecognitionEvent recognitionEvent);
    void delete(UUID uuid);

    Page<RecognitionEvent> findBetween(Long from, Long to, Pageable pageable);
    Page<RecognitionEvent> getEvents(FilterParams params, Pageable pageable);

    RecognitionEvent changeCarType(UUID id, CarType carType);
    EventFilterResponse getFilterData();

    RecognitionEvent correctGrnz(UUID uuid, String correctedGrnz);
    List<SendStatusCount> getStatistics();
}
