package xyz.lesnik.rabbittest.service;

import org.springframework.stereotype.Service;
import xyz.lesnik.rabbittest.config.ArchMapperConfig;
import xyz.lesnik.rabbittest.entity.RecognitionEvent;

/**
 * 30.12.2021
 * RecognitionEventHelper
 * 14:33
 */
@Service
public class RecognitionEventHelper {

    private final ArchMapperConfig mapperConfig;

    public RecognitionEventHelper(ArchMapperConfig mapperConfig) {
        this.mapperConfig = mapperConfig;
    }

    public void updateArchNames(RecognitionEvent recognitionEvent) {
        if(mapperConfig.getNames().containsKey(recognitionEvent.getName())){
            recognitionEvent.setOriginalAlias(recognitionEvent.getAlias());
            recognitionEvent.setAlias(mapperConfig.getNames().get(recognitionEvent.getName()));
        }
    }
}
