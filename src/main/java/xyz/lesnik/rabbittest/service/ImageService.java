package xyz.lesnik.rabbittest.service;

import xyz.lesnik.rabbittest.entity.Image;

import java.util.UUID;

/**
 * 14.12.2020
 * ImageService
 * 22:40
 */
public interface ImageService {
    Image create(Image image);
    Image get(UUID uuid);
    Iterable<Image> getAll();
    void delete(Image image);
}
