package xyz.lesnik.rabbittest.service.violation;

import xyz.lesnik.rabbittest.entity.RecognitionEvent;
import xyz.lesnik.rabbittest.entity.Violation;

import java.util.Optional;

/**
 * 19.12.2020
 * ViolationDetector
 * 20:45
 */
@FunctionalInterface
public interface ViolationDetector {
    Optional<Violation> detectViolation(RecognitionEvent recognitionEvent);
}
