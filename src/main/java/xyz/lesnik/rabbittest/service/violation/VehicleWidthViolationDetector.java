package xyz.lesnik.rabbittest.service.violation;

import org.springframework.stereotype.Component;
import xyz.lesnik.rabbittest.entity.RecognitionEvent;
import xyz.lesnik.rabbittest.entity.Violation;
import xyz.lesnik.rabbittest.entity.ViolationType;

import java.util.Optional;

/**
 * 19.12.2020
 * VehicleWidthViolationDetector
 * 20:59
 */
@Component
public class VehicleWidthViolationDetector implements ViolationDetector {

    private final static double WIDTH_LIMIT = 2.55;

    @Override
    public Optional<Violation> detectViolation(RecognitionEvent recognitionEvent) {
        if (recognitionEvent.getVehicleWidth() > WIDTH_LIMIT) {
            final double exceess = recognitionEvent.getVehicleWidth() - WIDTH_LIMIT;
            return Optional.of(new Violation(
                    ViolationType.WIDTH,
                    "Превышена максимальная ширина",
                    "Ширина превышена на: " + exceess, exceess));
        }
        return Optional.empty();
    }
}
