package xyz.lesnik.rabbittest.service.violation;

import org.springframework.stereotype.Component;
import xyz.lesnik.rabbittest.entity.RecognitionEvent;
import xyz.lesnik.rabbittest.entity.Violation;
import xyz.lesnik.rabbittest.entity.ViolationType;

import java.util.Optional;

/**
 * 19.12.2020
 * VehicleLengthViolationDetector
 * 21:05
 */
@Component
public class VehicleLengthViolationDetector implements ViolationDetector {
    private final static double LENGTH_LIMIT = 12.0;

    @Override
    public Optional<Violation> detectViolation(RecognitionEvent recognitionEvent) {
        if (recognitionEvent.getVehicleLength() > LENGTH_LIMIT) {
            final double excess = recognitionEvent.getVehicleLength() - LENGTH_LIMIT;
            return Optional.of(new Violation(
                    ViolationType.LENGTH,
                    "Превышена максимальная длина",
                    "Длина превышена на: " + excess, excess));
        }
        return Optional.empty();
    }
}
