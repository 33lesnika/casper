package xyz.lesnik.rabbittest.service.violation;

import org.springframework.stereotype.Component;
import xyz.lesnik.rabbittest.entity.RecognitionEvent;
import xyz.lesnik.rabbittest.entity.Violation;
import xyz.lesnik.rabbittest.entity.ViolationType;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * 19.12.2020
 * WeightPerAxisViolationDetector
 * 20:46
 */
@Component
public class WeightPerAxisViolationDetector implements ViolationDetector {

    private final Map<Integer, Integer> weightMap = new HashMap<>();

    @Override
    public Optional<Violation> detectViolation(RecognitionEvent recognitionEvent) {
        if (recognitionEvent.getAxlesCount() < 2) {
            return Optional.empty();
        }
        Integer limit = weightMap.get(recognitionEvent.getAxlesCount());
        if (limit == null) {
            limit = weightMap.values().stream().max(Integer::compareTo).orElse(0);
        }
        if (recognitionEvent.getGrossWeight() > limit) {
            final double excess = recognitionEvent.getGrossWeight() - limit;
            return Optional.of(
                    new Violation(
                            ViolationType.WEIGHT_PER_AXIS,
                            "Превышена максимальная общая масса",
                            "Масса превышена на: " + excess, excess));
        }
        return Optional.empty();
    }

    @PostConstruct
    private void init() {
        weightMap.put(2, 18_000);
        weightMap.put(3, 25_000);
        weightMap.put(4, 32_000);
        weightMap.put(5, 38_000);
        weightMap.put(6, 44_000);
    }
}
