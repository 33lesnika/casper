
package xyz.lesnik.rabbittest.soap;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *                 Информация по оси
 *             
 * 
 * <p>Java class for shaftInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="shaftInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="number" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="groupNumber" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="weight" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="weightLimit" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="leftWeight" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="rightWeight" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="isOverweight" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="whellCount" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="distance" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "shaftInfo", namespace = "http://www.ktk.kz/archControl/v1", propOrder = {
    "number",
    "groupNumber",
    "weight",
    "weightLimit",
    "leftWeight",
    "rightWeight",
    "isOverweight",
    "whellCount",
    "distance"
})
public class ShaftInfo {

    protected int number;
    protected int groupNumber;
    protected double weight;
    @XmlElementRef(name = "weightLimit", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> weightLimit;
    @XmlElementRef(name = "leftWeight", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> leftWeight;
    @XmlElementRef(name = "rightWeight", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> rightWeight;
    @XmlElementRef(name = "isOverweight", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isOverweight;
    @XmlElementRef(name = "whellCount", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> whellCount;
    protected double distance;

    /**
     * Gets the value of the number property.
     * 
     */
    public int getNumber() {
        return number;
    }

    /**
     * Sets the value of the number property.
     * 
     */
    public void setNumber(int value) {
        this.number = value;
    }

    /**
     * Gets the value of the groupNumber property.
     * 
     */
    public int getGroupNumber() {
        return groupNumber;
    }

    /**
     * Sets the value of the groupNumber property.
     * 
     */
    public void setGroupNumber(int value) {
        this.groupNumber = value;
    }

    /**
     * Gets the value of the weight property.
     * 
     */
    public double getWeight() {
        return weight;
    }

    /**
     * Sets the value of the weight property.
     * 
     */
    public void setWeight(double value) {
        this.weight = value;
    }

    /**
     * Gets the value of the weightLimit property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getWeightLimit() {
        return weightLimit;
    }

    /**
     * Sets the value of the weightLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setWeightLimit(JAXBElement<Double> value) {
        this.weightLimit = value;
    }

    /**
     * Gets the value of the leftWeight property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getLeftWeight() {
        return leftWeight;
    }

    /**
     * Sets the value of the leftWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setLeftWeight(JAXBElement<Double> value) {
        this.leftWeight = value;
    }

    /**
     * Gets the value of the rightWeight property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getRightWeight() {
        return rightWeight;
    }

    /**
     * Sets the value of the rightWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setRightWeight(JAXBElement<Double> value) {
        this.rightWeight = value;
    }

    /**
     * Gets the value of the isOverweight property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsOverweight() {
        return isOverweight;
    }

    /**
     * Sets the value of the isOverweight property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsOverweight(JAXBElement<Boolean> value) {
        this.isOverweight = value;
    }

    /**
     * Gets the value of the whellCount property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getWhellCount() {
        return whellCount;
    }

    /**
     * Sets the value of the whellCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setWhellCount(JAXBElement<Integer> value) {
        this.whellCount = value;
    }

    /**
     * Gets the value of the distance property.
     * 
     */
    public double getDistance() {
        return distance;
    }

    /**
     * Sets the value of the distance property.
     * 
     */
    public void setDistance(double value) {
        this.distance = value;
    }

}
