
package xyz.lesnik.rabbittest.soap;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the xyz.lesnik.rabbittest.soap package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ServerTimeResponse_QNAME = new QName("http://www.ktk.kz/archControl/v1", "serverTimeResponse");
    private final static QName _PhotoIsDebug_QNAME = new QName("", "isDebug");
    private final static QName _ArchRecordIsOverweightPartial_QNAME = new QName("", "isOverweightPartial");
    private final static QName _ArchRecordIsExceededWidth_QNAME = new QName("", "isExceededWidth");
    private final static QName _ArchRecordHasVehicleNumberPhoto_QNAME = new QName("", "hasVehicleNumberPhoto");
    private final static QName _ArchRecordUncorrectType_QNAME = new QName("", "uncorrectType");
    private final static QName _ArchRecordVehicleNumberRear_QNAME = new QName("", "vehicleNumberRear");
    private final static QName _ArchRecordWheelBase_QNAME = new QName("", "wheelBase");
    private final static QName _ArchRecordIsOverweight_QNAME = new QName("", "isOverweight");
    private final static QName _ArchRecordWeightLimit_QNAME = new QName("", "weightLimit");
    private final static QName _ArchRecordIsNonStandard_QNAME = new QName("", "isNonStandard");
    private final static QName _ArchRecordIsExceededLength_QNAME = new QName("", "isExceededLength");
    private final static QName _ArchRecordIsExceededOperatingRange_QNAME = new QName("", "isExceededOperatingRange");
    private final static QName _ArchRecordIsOverweightGross_QNAME = new QName("", "isOverweightGross");
    private final static QName _ArchRecordCountryCode_QNAME = new QName("", "countryCode");
    private final static QName _ArchRecordIsExceededHeight_QNAME = new QName("", "isExceededHeight");
    private final static QName _ArchRecordOverWeight_QNAME = new QName("", "overWeight");
    private final static QName _ArchRecordVehicleCountry_QNAME = new QName("", "vehicleCountry");
    private final static QName _ArchRecordIsWrongDirection_QNAME = new QName("", "isWrongDirection");
    private final static QName _ArchRecordIsOversized_QNAME = new QName("", "isOversized");
    private final static QName _ArchRecordLane_QNAME = new QName("", "lane");
    private final static QName _ArchRecordVehicleType_QNAME = new QName("", "vehicleType");
    private final static QName _ShaftInfoLeftWeight_QNAME = new QName("", "leftWeight");
    private final static QName _ShaftInfoWhellCount_QNAME = new QName("", "whellCount");
    private final static QName _ShaftInfoRightWeight_QNAME = new QName("", "rightWeight");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: xyz.lesnik.rabbittest.soap
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ArchRecordResponse }
     * 
     */
    public ArchRecordResponse createArchRecordResponse() {
        return new ArchRecordResponse();
    }

    /**
     * Create an instance of {@link ArchRecordRequest }
     * 
     */
    public ArchRecordRequest createArchRecordRequest() {
        return new ArchRecordRequest();
    }

    /**
     * Create an instance of {@link ArchRecord }
     * 
     */
    public ArchRecord createArchRecord() {
        return new ArchRecord();
    }

    /**
     * Create an instance of {@link ShaftInfo }
     * 
     */
    public ShaftInfo createShaftInfo() {
        return new ShaftInfo();
    }

    /**
     * Create an instance of {@link Photo }
     * 
     */
    public Photo createPhoto() {
        return new Photo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ktk.kz/archControl/v1", name = "serverTimeResponse")
    public JAXBElement<XMLGregorianCalendar> createServerTimeResponse(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ServerTimeResponse_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "isDebug", scope = Photo.class)
    public JAXBElement<Boolean> createPhotoIsDebug(Boolean value) {
        return new JAXBElement<Boolean>(_PhotoIsDebug_QNAME, Boolean.class, Photo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "isOverweightPartial", scope = ArchRecord.class)
    public JAXBElement<Boolean> createArchRecordIsOverweightPartial(Boolean value) {
        return new JAXBElement<Boolean>(_ArchRecordIsOverweightPartial_QNAME, Boolean.class, ArchRecord.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "isExceededWidth", scope = ArchRecord.class)
    public JAXBElement<Boolean> createArchRecordIsExceededWidth(Boolean value) {
        return new JAXBElement<Boolean>(_ArchRecordIsExceededWidth_QNAME, Boolean.class, ArchRecord.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "hasVehicleNumberPhoto", scope = ArchRecord.class)
    public JAXBElement<Boolean> createArchRecordHasVehicleNumberPhoto(Boolean value) {
        return new JAXBElement<Boolean>(_ArchRecordHasVehicleNumberPhoto_QNAME, Boolean.class, ArchRecord.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "uncorrectType", scope = ArchRecord.class)
    public JAXBElement<Integer> createArchRecordUncorrectType(Integer value) {
        return new JAXBElement<Integer>(_ArchRecordUncorrectType_QNAME, Integer.class, ArchRecord.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vehicleNumberRear", scope = ArchRecord.class)
    public JAXBElement<String> createArchRecordVehicleNumberRear(String value) {
        return new JAXBElement<String>(_ArchRecordVehicleNumberRear_QNAME, String.class, ArchRecord.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "wheelBase", scope = ArchRecord.class)
    public JAXBElement<Integer> createArchRecordWheelBase(Integer value) {
        return new JAXBElement<Integer>(_ArchRecordWheelBase_QNAME, Integer.class, ArchRecord.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "isOverweight", scope = ArchRecord.class)
    public JAXBElement<Boolean> createArchRecordIsOverweight(Boolean value) {
        return new JAXBElement<Boolean>(_ArchRecordIsOverweight_QNAME, Boolean.class, ArchRecord.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "weightLimit", scope = ArchRecord.class)
    public JAXBElement<Double> createArchRecordWeightLimit(Double value) {
        return new JAXBElement<Double>(_ArchRecordWeightLimit_QNAME, Double.class, ArchRecord.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "isNonStandard", scope = ArchRecord.class)
    public JAXBElement<Boolean> createArchRecordIsNonStandard(Boolean value) {
        return new JAXBElement<Boolean>(_ArchRecordIsNonStandard_QNAME, Boolean.class, ArchRecord.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "isExceededLength", scope = ArchRecord.class)
    public JAXBElement<Boolean> createArchRecordIsExceededLength(Boolean value) {
        return new JAXBElement<Boolean>(_ArchRecordIsExceededLength_QNAME, Boolean.class, ArchRecord.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "isExceededOperatingRange", scope = ArchRecord.class)
    public JAXBElement<Boolean> createArchRecordIsExceededOperatingRange(Boolean value) {
        return new JAXBElement<Boolean>(_ArchRecordIsExceededOperatingRange_QNAME, Boolean.class, ArchRecord.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "isOverweightGross", scope = ArchRecord.class)
    public JAXBElement<Boolean> createArchRecordIsOverweightGross(Boolean value) {
        return new JAXBElement<Boolean>(_ArchRecordIsOverweightGross_QNAME, Boolean.class, ArchRecord.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "countryCode", scope = ArchRecord.class)
    public JAXBElement<String> createArchRecordCountryCode(String value) {
        return new JAXBElement<String>(_ArchRecordCountryCode_QNAME, String.class, ArchRecord.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "isExceededHeight", scope = ArchRecord.class)
    public JAXBElement<Boolean> createArchRecordIsExceededHeight(Boolean value) {
        return new JAXBElement<Boolean>(_ArchRecordIsExceededHeight_QNAME, Boolean.class, ArchRecord.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "overWeight", scope = ArchRecord.class)
    public JAXBElement<Double> createArchRecordOverWeight(Double value) {
        return new JAXBElement<Double>(_ArchRecordOverWeight_QNAME, Double.class, ArchRecord.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vehicleCountry", scope = ArchRecord.class)
    public JAXBElement<String> createArchRecordVehicleCountry(String value) {
        return new JAXBElement<String>(_ArchRecordVehicleCountry_QNAME, String.class, ArchRecord.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "isWrongDirection", scope = ArchRecord.class)
    public JAXBElement<Boolean> createArchRecordIsWrongDirection(Boolean value) {
        return new JAXBElement<Boolean>(_ArchRecordIsWrongDirection_QNAME, Boolean.class, ArchRecord.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "isOversized", scope = ArchRecord.class)
    public JAXBElement<Boolean> createArchRecordIsOversized(Boolean value) {
        return new JAXBElement<Boolean>(_ArchRecordIsOversized_QNAME, Boolean.class, ArchRecord.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "lane", scope = ArchRecord.class)
    public JAXBElement<Integer> createArchRecordLane(Integer value) {
        return new JAXBElement<Integer>(_ArchRecordLane_QNAME, Integer.class, ArchRecord.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vehicleType", scope = ArchRecord.class)
    public JAXBElement<Integer> createArchRecordVehicleType(Integer value) {
        return new JAXBElement<Integer>(_ArchRecordVehicleType_QNAME, Integer.class, ArchRecord.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "leftWeight", scope = ShaftInfo.class)
    public JAXBElement<Double> createShaftInfoLeftWeight(Double value) {
        return new JAXBElement<Double>(_ShaftInfoLeftWeight_QNAME, Double.class, ShaftInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "whellCount", scope = ShaftInfo.class)
    public JAXBElement<Integer> createShaftInfoWhellCount(Integer value) {
        return new JAXBElement<Integer>(_ShaftInfoWhellCount_QNAME, Integer.class, ShaftInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "rightWeight", scope = ShaftInfo.class)
    public JAXBElement<Double> createShaftInfoRightWeight(Double value) {
        return new JAXBElement<Double>(_ShaftInfoRightWeight_QNAME, Double.class, ShaftInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "isOverweight", scope = ShaftInfo.class)
    public JAXBElement<Boolean> createShaftInfoIsOverweight(Boolean value) {
        return new JAXBElement<Boolean>(_ArchRecordIsOverweight_QNAME, Boolean.class, ShaftInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "weightLimit", scope = ShaftInfo.class)
    public JAXBElement<Double> createShaftInfoWeightLimit(Double value) {
        return new JAXBElement<Double>(_ArchRecordWeightLimit_QNAME, Double.class, ShaftInfo.class, value);
    }

}
