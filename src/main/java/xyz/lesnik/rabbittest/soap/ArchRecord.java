
package xyz.lesnik.rabbittest.soap;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *                 Запись с контрольной арки
 *             
 * 
 * <p>Java class for archRecord complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="archRecord">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="archCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="createdBy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="softwareVersion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="credence" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="wayType" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="direction" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="lane" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="recordDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="vehicleNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="vehicleCountry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vehicleNumberRear" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hasVehicleNumberPhoto" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="vehicleType" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="uncorrectType" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="speed" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="length" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="width" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="height" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="wheelBase" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="weight" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="weightLimit" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="overWeight" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="isOverweightGross" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="isOverweightPartial" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="isOverweight" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="isExceededWidth" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="isExceededHeight" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="isExceededLength" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="isOversized" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="isWrongDirection" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="isNonStandard" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="isExceededOperatingRange" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="signature" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *         &lt;element name="photo" type="{http://www.ktk.kz/archControl/v1}photo" maxOccurs="unbounded"/>
 *         &lt;element name="shaftInfo" type="{http://www.ktk.kz/archControl/v1}shaftInfo" maxOccurs="unbounded" minOccurs="2"/>
 *         &lt;element name="countryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "archRecord", namespace = "http://www.ktk.kz/archControl/v1", propOrder = {
    "archCode",
    "createdBy",
    "softwareVersion",
    "credence",
    "wayType",
    "direction",
    "lane",
    "recordDate",
    "vehicleNumber",
    "vehicleCountry",
    "vehicleNumberRear",
    "hasVehicleNumberPhoto",
    "vehicleType",
    "uncorrectType",
    "speed",
    "length",
    "width",
    "height",
    "wheelBase",
    "weight",
    "weightLimit",
    "overWeight",
    "isOverweightGross",
    "isOverweightPartial",
    "isOverweight",
    "isExceededWidth",
    "isExceededHeight",
    "isExceededLength",
    "isOversized",
    "isWrongDirection",
    "isNonStandard",
    "isExceededOperatingRange",
    "signature",
    "photo",
    "shaftInfo",
    "countryCode"
})
public class ArchRecord {

    @XmlElement(required = true)
    protected String archCode;
    protected String createdBy;
    protected String softwareVersion;
    protected int credence;
    protected int wayType;
    protected int direction;
    @XmlElementRef(name = "lane", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> lane;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar recordDate;
    @XmlElement(required = true)
    protected String vehicleNumber;
    @XmlElementRef(name = "vehicleCountry", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vehicleCountry;
    @XmlElementRef(name = "vehicleNumberRear", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vehicleNumberRear;
    @XmlElementRef(name = "hasVehicleNumberPhoto", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> hasVehicleNumberPhoto;
    @XmlElementRef(name = "vehicleType", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> vehicleType;
    @XmlElementRef(name = "uncorrectType", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> uncorrectType;
    protected double speed;
    protected int length;
    protected int width;
    protected int height;
    @XmlElementRef(name = "wheelBase", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> wheelBase;
    protected double weight;
    @XmlElementRef(name = "weightLimit", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> weightLimit;
    @XmlElementRef(name = "overWeight", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> overWeight;
    @XmlElementRef(name = "isOverweightGross", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isOverweightGross;
    @XmlElementRef(name = "isOverweightPartial", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isOverweightPartial;
    @XmlElementRef(name = "isOverweight", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isOverweight;
    @XmlElementRef(name = "isExceededWidth", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isExceededWidth;
    @XmlElementRef(name = "isExceededHeight", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isExceededHeight;
    @XmlElementRef(name = "isExceededLength", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isExceededLength;
    @XmlElementRef(name = "isOversized", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isOversized;
    @XmlElementRef(name = "isWrongDirection", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isWrongDirection;
    @XmlElementRef(name = "isNonStandard", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isNonStandard;
    @XmlElementRef(name = "isExceededOperatingRange", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isExceededOperatingRange;
    @XmlElement(required = true)
    protected byte[] signature;
    @XmlElement(required = true)
    protected List<Photo> photo;
    @XmlElement(required = true)
    protected List<ShaftInfo> shaftInfo;
    @XmlElementRef(name = "countryCode", type = JAXBElement.class, required = false)
    protected JAXBElement<String> countryCode;

    /**
     * Gets the value of the archCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArchCode() {
        return archCode;
    }

    /**
     * Sets the value of the archCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArchCode(String value) {
        this.archCode = value;
    }

    /**
     * Gets the value of the createdBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Sets the value of the createdBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreatedBy(String value) {
        this.createdBy = value;
    }

    /**
     * Gets the value of the softwareVersion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSoftwareVersion() {
        return softwareVersion;
    }

    /**
     * Sets the value of the softwareVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSoftwareVersion(String value) {
        this.softwareVersion = value;
    }

    /**
     * Gets the value of the credence property.
     * 
     */
    public int getCredence() {
        return credence;
    }

    /**
     * Sets the value of the credence property.
     * 
     */
    public void setCredence(int value) {
        this.credence = value;
    }

    /**
     * Gets the value of the wayType property.
     * 
     */
    public int getWayType() {
        return wayType;
    }

    /**
     * Sets the value of the wayType property.
     * 
     */
    public void setWayType(int value) {
        this.wayType = value;
    }

    /**
     * Gets the value of the direction property.
     * 
     */
    public int getDirection() {
        return direction;
    }

    /**
     * Sets the value of the direction property.
     * 
     */
    public void setDirection(int value) {
        this.direction = value;
    }

    /**
     * Gets the value of the lane property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getLane() {
        return lane;
    }

    /**
     * Sets the value of the lane property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setLane(JAXBElement<Integer> value) {
        this.lane = value;
    }

    /**
     * Gets the value of the recordDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRecordDate() {
        return recordDate;
    }

    /**
     * Sets the value of the recordDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRecordDate(XMLGregorianCalendar value) {
        this.recordDate = value;
    }

    /**
     * Gets the value of the vehicleNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehicleNumber() {
        return vehicleNumber;
    }

    /**
     * Sets the value of the vehicleNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehicleNumber(String value) {
        this.vehicleNumber = value;
    }

    /**
     * Gets the value of the vehicleCountry property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVehicleCountry() {
        return vehicleCountry;
    }

    /**
     * Sets the value of the vehicleCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVehicleCountry(JAXBElement<String> value) {
        this.vehicleCountry = value;
    }

    /**
     * Gets the value of the vehicleNumberRear property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVehicleNumberRear() {
        return vehicleNumberRear;
    }

    /**
     * Sets the value of the vehicleNumberRear property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVehicleNumberRear(JAXBElement<String> value) {
        this.vehicleNumberRear = value;
    }

    /**
     * Gets the value of the hasVehicleNumberPhoto property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getHasVehicleNumberPhoto() {
        return hasVehicleNumberPhoto;
    }

    /**
     * Sets the value of the hasVehicleNumberPhoto property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setHasVehicleNumberPhoto(JAXBElement<Boolean> value) {
        this.hasVehicleNumberPhoto = value;
    }

    /**
     * Gets the value of the vehicleType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getVehicleType() {
        return vehicleType;
    }

    /**
     * Sets the value of the vehicleType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setVehicleType(JAXBElement<Integer> value) {
        this.vehicleType = value;
    }

    /**
     * Gets the value of the uncorrectType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getUncorrectType() {
        return uncorrectType;
    }

    /**
     * Sets the value of the uncorrectType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setUncorrectType(JAXBElement<Integer> value) {
        this.uncorrectType = value;
    }

    /**
     * Gets the value of the speed property.
     * 
     */
    public double getSpeed() {
        return speed;
    }

    /**
     * Sets the value of the speed property.
     * 
     */
    public void setSpeed(double value) {
        this.speed = value;
    }

    /**
     * Gets the value of the length property.
     * 
     */
    public int getLength() {
        return length;
    }

    /**
     * Sets the value of the length property.
     * 
     */
    public void setLength(int value) {
        this.length = value;
    }

    /**
     * Gets the value of the width property.
     * 
     */
    public int getWidth() {
        return width;
    }

    /**
     * Sets the value of the width property.
     * 
     */
    public void setWidth(int value) {
        this.width = value;
    }

    /**
     * Gets the value of the height property.
     * 
     */
    public int getHeight() {
        return height;
    }

    /**
     * Sets the value of the height property.
     * 
     */
    public void setHeight(int value) {
        this.height = value;
    }

    /**
     * Gets the value of the wheelBase property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getWheelBase() {
        return wheelBase;
    }

    /**
     * Sets the value of the wheelBase property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setWheelBase(JAXBElement<Integer> value) {
        this.wheelBase = value;
    }

    /**
     * Gets the value of the weight property.
     * 
     */
    public double getWeight() {
        return weight;
    }

    /**
     * Sets the value of the weight property.
     * 
     */
    public void setWeight(double value) {
        this.weight = value;
    }

    /**
     * Gets the value of the weightLimit property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getWeightLimit() {
        return weightLimit;
    }

    /**
     * Sets the value of the weightLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setWeightLimit(JAXBElement<Double> value) {
        this.weightLimit = value;
    }

    /**
     * Gets the value of the overWeight property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getOverWeight() {
        return overWeight;
    }

    /**
     * Sets the value of the overWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setOverWeight(JAXBElement<Double> value) {
        this.overWeight = value;
    }

    /**
     * Gets the value of the isOverweightGross property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsOverweightGross() {
        return isOverweightGross;
    }

    /**
     * Sets the value of the isOverweightGross property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsOverweightGross(JAXBElement<Boolean> value) {
        this.isOverweightGross = value;
    }

    /**
     * Gets the value of the isOverweightPartial property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsOverweightPartial() {
        return isOverweightPartial;
    }

    /**
     * Sets the value of the isOverweightPartial property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsOverweightPartial(JAXBElement<Boolean> value) {
        this.isOverweightPartial = value;
    }

    /**
     * Gets the value of the isOverweight property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsOverweight() {
        return isOverweight;
    }

    /**
     * Sets the value of the isOverweight property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsOverweight(JAXBElement<Boolean> value) {
        this.isOverweight = value;
    }

    /**
     * Gets the value of the isExceededWidth property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsExceededWidth() {
        return isExceededWidth;
    }

    /**
     * Sets the value of the isExceededWidth property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsExceededWidth(JAXBElement<Boolean> value) {
        this.isExceededWidth = value;
    }

    /**
     * Gets the value of the isExceededHeight property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsExceededHeight() {
        return isExceededHeight;
    }

    /**
     * Sets the value of the isExceededHeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsExceededHeight(JAXBElement<Boolean> value) {
        this.isExceededHeight = value;
    }

    /**
     * Gets the value of the isExceededLength property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsExceededLength() {
        return isExceededLength;
    }

    /**
     * Sets the value of the isExceededLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsExceededLength(JAXBElement<Boolean> value) {
        this.isExceededLength = value;
    }

    /**
     * Gets the value of the isOversized property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsOversized() {
        return isOversized;
    }

    /**
     * Sets the value of the isOversized property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsOversized(JAXBElement<Boolean> value) {
        this.isOversized = value;
    }

    /**
     * Gets the value of the isWrongDirection property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsWrongDirection() {
        return isWrongDirection;
    }

    /**
     * Sets the value of the isWrongDirection property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsWrongDirection(JAXBElement<Boolean> value) {
        this.isWrongDirection = value;
    }

    /**
     * Gets the value of the isNonStandard property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsNonStandard() {
        return isNonStandard;
    }

    /**
     * Sets the value of the isNonStandard property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsNonStandard(JAXBElement<Boolean> value) {
        this.isNonStandard = value;
    }

    /**
     * Gets the value of the isExceededOperatingRange property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsExceededOperatingRange() {
        return isExceededOperatingRange;
    }

    /**
     * Sets the value of the isExceededOperatingRange property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsExceededOperatingRange(JAXBElement<Boolean> value) {
        this.isExceededOperatingRange = value;
    }

    /**
     * Gets the value of the signature property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getSignature() {
        return signature;
    }

    /**
     * Sets the value of the signature property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setSignature(byte[] value) {
        this.signature = value;
    }

    /**
     * Gets the value of the photo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the photo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPhoto().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Photo }
     * 
     * 
     */
    public List<Photo> getPhoto() {
        if (photo == null) {
            photo = new ArrayList<Photo>();
        }
        return this.photo;
    }

    /**
     * Gets the value of the shaftInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shaftInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShaftInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShaftInfo }
     * 
     * 
     */
    public List<ShaftInfo> getShaftInfo() {
        if (shaftInfo == null) {
            shaftInfo = new ArrayList<ShaftInfo>();
        }
        return this.shaftInfo;
    }

    /**
     * Gets the value of the countryCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCountryCode() {
        return countryCode;
    }

    /**
     * Sets the value of the countryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCountryCode(JAXBElement<String> value) {
        this.countryCode = value;
    }

}
