package xyz.lesnik.rabbittest;

import com.rabbitmq.client.Connection;
import org.springframework.stereotype.Component;
import xyz.lesnik.rabbittest.entity.DataSourceSettings;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiConsumer;

/**
 * 23.04.2021
 * RabbitConnectionHolder
 * 21:26
 */
@Component
public class RabbitConnectionHolder{
    private final Map<Long, Connection> connectionMap = new ConcurrentHashMap<>();

    public void store(Long id, Connection conn){
        connectionMap.put(id, conn);
    }

    public Connection get(Long id){
        return connectionMap.get(id);
    }

    public void clearAll(){
        connectionMap.clear();
    }

    public Collection<Connection> values(){
        return connectionMap.values();
    }

    public boolean isConnectionAlive(DataSourceSettings settings) {
        var connection = connectionMap.get(settings.getId());
        return connection != null && connection.isOpen();
    }

    public void forEach(BiConsumer<? super Long, ? super Connection> action){
        connectionMap.forEach(action);
    }

    public Connection replace(Long key, Connection connection){
        return connectionMap.replace(key, connection);
    }

}
