package xyz.lesnik.rabbittest.vshep;

import lombok.Data;
import lombok.SneakyThrows;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.nio.file.Files;
import java.nio.file.Path;

/**
 * 25.02.2021
 * SoapVshepConfig
 * 06:01
 */
@Configuration
@Profile("vshep")
@Data
@ConfigurationProperties("vshep")
public class SoapVshepConfig {
    String url;
    String keystore;
    String password;
    String xmlTemplate;

    @SneakyThrows
    @Bean
    public TemplateXmlView xmlView(){
        String template = Files.readString(Path.of(xmlTemplate));
        return new TemplateXmlView(template);
    }

    @Bean
    public NewXmlSigner xmlSigner(){
        return new NewXmlSigner(getKeystore(), getPassword());
    }
}
