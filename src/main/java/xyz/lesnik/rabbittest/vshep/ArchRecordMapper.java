package xyz.lesnik.rabbittest.vshep;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import xyz.lesnik.rabbittest.entity.*;
import xyz.lesnik.rabbittest.service.ImageService;
import xyz.lesnik.rabbittest.soap.ArchRecord;
import xyz.lesnik.rabbittest.soap.Photo;
import xyz.lesnik.rabbittest.soap.ShaftInfo;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.namespace.QName;
import java.util.*;

/**
 * 08.09.2021
 * ArchRecordMapper
 * 16:58
 */
@Service
@Profile("vshep")
@Slf4j
public class ArchRecordMapper {

    private final ImageService imageService;

    QName qname = new QName("http://www.ktk.kz/archControl/v1", "arch");

    public ArchRecordMapper(ImageService imageService) {
        this.imageService = imageService;
    }

    public ArchRecord toRecord(RecognitionEvent event) {
        QName qname = new QName("http://www.ktk.kz/archControl/v1", "arch");
        ArchRecord record = new ArchRecord();
        record.setArchCode(event.getName());
        record.setCreatedBy("06010102-3000-4250-0000-000000000011");
        record.setSoftwareVersion("2.3.1.0");
        record.setCredence(0);
        record.setWayType(event.getDirection());
        record.setDirection(event.getDirection() + 1);
//        record.setLane(new JAXBElement<>()1);
        GregorianCalendar c = new GregorianCalendar();
        c.setTime(new Date(event.getTimestamp()));
        try {
            record.setRecordDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(c));
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }
        record.setVehicleNumber(event.getGrnz());
        record.setVehicleCountry(new JAXBElement<>(qname, String.class, "KAZ"));
        record.setVehicleNumberRear(toJaxb(event.getGrnz()));
        record.setHasVehicleNumberPhoto(toJaxb(false));
        if (event.getPlatefrontId() != null) {
            record.setHasVehicleNumberPhoto(toJaxb(true));
        }
        record.setVehicleType(toJaxb(event.getClassWIM()));
        record.setUncorrectType(toJaxb(0));
        record.setSpeed(event.getVelocity());
        record.setLength((int) (event.getVehicleLength() * 100));
        record.setWidth((int) (event.getVehicleWidth() * 100));
        record.setHeight((int) (event.getVehicleHeight() * 100));
        record.setWheelBase(toJaxb((int) (event.getWheelBase() * 100)));
        record.setWeight(event.getGrossWeight());
        record.setWeightLimit(toJaxb(0d));
        boolean isOverweight = event.getViolations() != null && event.getViolations().stream().anyMatch(x -> x.getType() == ViolationType.WEIGHT_PER_AXIS);
        if (isOverweight) {
            final double overweight = event.getViolations().stream()
                    .filter(x -> x.getType() == ViolationType.WEIGHT_PER_AXIS)
                    .mapToDouble(Violation::getValue)
                    .max().orElse(0d);
            record.setOverWeight(toJaxb(overweight));
        }
        record.setIsOverweightGross(toJaxb(isOverweight));
        record.setIsOverweightPartial(toJaxb(false));
        record.setIsOverweight(toJaxb(isOverweight));
        var isExceededWidth = event.getViolations() != null && event.getViolations().stream().anyMatch(x -> x.getType() == ViolationType.WIDTH);
        record.setIsExceededWidth(toJaxb(isExceededWidth));
        var isExceededHeight = false;
        record.setIsExceededHeight(toJaxb(isExceededHeight));
        var isExceededLength = event.getViolations() != null && event.getViolations().stream().anyMatch(x -> x.getType() == ViolationType.LENGTH);
        record.setIsExceededLength(toJaxb(isExceededLength));
        var isOversized = isExceededHeight || isExceededLength || isExceededWidth;
        record.setIsOversized(toJaxb(isOversized));
        record.setIsWrongDirection(toJaxb(false));
        record.setIsNonStandard(toJaxb(false));
        record.setIsExceededOperatingRange(toJaxb(false));
        record.setCountryCode(toJaxb("KAZ"));
        record.setSignature(new byte[]{});
        tyToAddImages(record.getPhoto(), event.getAnprfrontId(), event.getOvcfrontId(),
                event.getPlatefrontId(), event.getAnprrearId(), event.getPlaterearId());
        tyToAddShafts(record.getShaftInfo(), event.getAxles());
        return record;
    }

    private JAXBElement<String> toJaxb(String value) {
        return new JAXBElement<>(qname, String.class, value);
    }

    private JAXBElement<Boolean> toJaxb(Boolean value) {
        return new JAXBElement<>(qname, Boolean.class, value);
    }

    private JAXBElement<Integer> toJaxb(Integer value) {
        return new JAXBElement<>(qname, Integer.class, value);
    }

    private JAXBElement<Double> toJaxb(Double value) {
        return new JAXBElement<>(qname, Double.class, value);
    }

    private void tyToAddImages(List<Photo> photos, UUID... uuids) {
        for (UUID uuid : uuids) {
            if (uuid == null) continue;
            try {
                final Image image = imageService.get(uuid);
                Photo photo = new Photo();
                photo.setData(image.getData());
                photo.setIsDebug(toJaxb(false));
                photos.add(photo);
            } catch (Exception e) {
                log.error("Cannot add photo to xml, reason: {}", e.getMessage());
            }
        }
    }

    private void tyToAddShafts(List<ShaftInfo> shafts, List<Axle> axles) {
        if (axles == null) return;
        for (Axle axle : axles) {
            shafts.add(toShaftInfo(axle));
        }
    }

    private ShaftInfo toShaftInfo(Axle axle) {
        ShaftInfo shaft = new ShaftInfo();
        shaft.setNumber(axle.getNumber());
        shaft.setDistance(axle.getDistance());
        shaft.setWeight(axle.getWeight());
        shaft.setGroupNumber(1);
        return shaft;
    }
}
