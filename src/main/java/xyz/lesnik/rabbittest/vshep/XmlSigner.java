package xyz.lesnik.rabbittest.vshep;

import kz.gov.pki.kalkan.asn1.pkcs.PKCSObjectIdentifiers;
import kz.gov.pki.kalkan.jce.provider.KalkanProvider;
import kz.gov.pki.kalkan.xmldsig.KncaXS;
import lombok.SneakyThrows;
//import org.apache.wss4j.common.WSS4JConstants;
//import org.apache.wss4j.common.token.SecurityTokenReference;
//import org.apache.wss4j.dom.WSConstants;
//import org.apache.wss4j.dom.message.WSSecHeader;
//import org.apache.wss4j.dom.message.WSSecUsernameToken;
import org.apache.ws.security.message.WSSecHeader;
import org.apache.ws.security.message.token.SecurityTokenReference;
import org.apache.xml.security.c14n.Canonicalizer;
import org.apache.xml.security.encryption.XMLCipherParameters;
import org.apache.xml.security.signature.XMLSignature;
import org.apache.xml.security.transforms.Transforms;
import org.apache.xml.security.utils.Constants;
import org.apache.xml.security.utils.XMLUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.namespace.QName;
import javax.xml.soap.*;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.security.*;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.UUID;

/**
 * 25.02.2021
 * XmlSigner
 * 06:21
 */
public class XmlSigner {

    private final String keyStoreName;
    private final String keyStorePassword;

    public XmlSigner(String keyStoreName, String keyStorePassword) {
        this.keyStoreName = keyStoreName;
        this.keyStorePassword = keyStorePassword;
    }
//
//    public String sign(String envelope) throws XmlSigningException {
//
//        KalkanProvider kalkanProvider = new KalkanProvider();
//        Security.addProvider(kalkanProvider);
//        KncaXS.loadXMLSecurity();
//
//        try {
//            final String signMethod;
//            final String digestMethod;
//            InputStream is = new ByteArrayInputStream(envelope.getBytes());
//
//            SOAPMessage msg = MessageFactory.newInstance().createMessage(null, is);
//
//            SOAPEnvelope env = msg.getSOAPPart().getEnvelope();
//            SOAPBody body = env.getBody();
//
//            String bodyId = "id-" + UUID.randomUUID().toString();
//            var body_elem = body.addAttribute(new QName(WSS4JConstants.WSU_NS, "Id", WSS4JConstants.WSU_PREFIX), bodyId);
//            var id = body_elem.getAttributeNodeNS(WSS4JConstants.WSU_NS, "Id");
//            body.setIdAttributeNode(id, true);
//
//            SOAPHeader header = env.getHeader();
//            if (header == null) {
//                header = env.addHeader();
//            }
//            KeyStore store = KeyStore.getInstance("PKCS12", KalkanProvider.PROVIDER_NAME);
//            InputStream inputStream;
//            inputStream = AccessController.doPrivileged(new PrivilegedExceptionAction<InputStream>() {
//                @Override
//                public FileInputStream run() throws Exception {
//                    return new FileInputStream(keyStoreName);
//                }
//            });
//            store.load(inputStream, keyStorePassword.toCharArray());
//            Enumeration<String> als = store.aliases();
//            String alias = null;
//            while (als.hasMoreElements()) {
//                alias = als.nextElement();
//            }
//            final PrivateKey privateKey = (PrivateKey) store.getKey(alias, keyStorePassword.toCharArray());
//            final X509Certificate x509Certificate = (X509Certificate) store.getCertificate(alias);
//            String sigAlgOid = x509Certificate.getSigAlgOID();
//            if (sigAlgOid.equals(PKCSObjectIdentifiers.sha1WithRSAEncryption.getId())) {
//                signMethod = Constants.MoreAlgorithmsSpecNS + "rsa-sha1";
//                digestMethod = Constants.MoreAlgorithmsSpecNS + "sha1";
//            } else if (sigAlgOid.equals(PKCSObjectIdentifiers.sha256WithRSAEncryption.getId())) {
//                signMethod = Constants.MoreAlgorithmsSpecNS + "rsa-sha256";
//                digestMethod = XMLCipherParameters.SHA256;
//            } else {
//                signMethod = Constants.MoreAlgorithmsSpecNS + "gost34310-gost34311";
//                digestMethod = Constants.MoreAlgorithmsSpecNS + "gost34311";
//            }
//
//            Document doc = env.getOwnerDocument();
//            Transforms transforms = new Transforms(doc);
//            transforms.addTransform(Transforms.TRANSFORM_C14N_EXCL_OMIT_COMMENTS);
//
//            Element c14nMethod = XMLUtils.createElementInSignatureSpace(doc, "CanonicalizationMethod");
//            c14nMethod.setAttributeNS(null, "Algorithm", Canonicalizer.ALGO_ID_C14N_EXCL_OMIT_COMMENTS);
//
//            Element signatureMethod = XMLUtils.createElementInSignatureSpace(doc, "SignatureMethod");
//            signatureMethod.setAttributeNS(null, "Algorithm", signMethod);
//
//            XMLSignature sig = new XMLSignature(env.getOwnerDocument(), "", signatureMethod, c14nMethod);
//
//            sig.addDocument("#" + bodyId, transforms, digestMethod);
//            sig.getSignedInfo().getSignatureMethodElement().setNodeValue(Transforms.TRANSFORM_C14N_EXCL_OMIT_COMMENTS);
//
//            WSSecHeader secHeader = new WSSecHeader(doc);
//            secHeader.setMustUnderstand(true);
//            var element = secHeader.insertSecurityHeader();
//            header.appendChild(element);
//
////            WSSecUsernameToken usernameToken = new WSSecUsernameToken(secHeader);
////            usernameToken.setPasswordType(WSConstants.PASSWORD_DIGEST);
////            usernameToken.setUserInfo("SVP", "Sistemavzimaniyaplaty");
////            usernameToken.addCreated();
////            usernameToken.addNonce();
////            usernameToken.prepare();
////            usernameToken.appendToHeader();
//
//            SecurityTokenReference reference = new SecurityTokenReference(sig.getKeyInfo().getDocument());
//            reference.addWSSENamespace();
//            reference.setKeyIdentifier(x509Certificate);
////
//            sig.getKeyInfo().addUnknownElement(reference.getElement());
//            sig.sign(privateKey);
////            sig.addKeyInfo(x509Certificate);
//
//            var element1 = sig.getElement();
//            var importNode = secHeader.getSecurityHeaderElement().getOwnerDocument().importNode(element1, true);
//            secHeader.getSecurityHeaderElement().appendChild(importNode);
//
//            return xmlToString(doc);
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw new XmlSigningException(e);
//        }
//    }
//
//    @SneakyThrows
//    public String xmlToString(Document doc) {
//        TransformerFactory tf = TransformerFactory.newInstance();
//        Transformer transformer = tf.newTransformer();
//        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
//        transformer.setOutputProperty(OutputKeys.INDENT, "no");
//        StringWriter writer = new StringWriter();
//        transformer.transform(new DOMSource(doc), new StreamResult(writer));
//        var s = writer.toString();
//        BufferedReader br = new BufferedReader(new StringReader(s));
//        StringBuilder sb = new StringBuilder();
//        String line;
//        while ((line = br.readLine()) != null) {
//            line = line.trim();
//            if (line.length() != 0) {
//                sb.append(line);
//            }
//        }
//        return sb.toString();
//    }
//
//
//    public boolean addHeadSecurity (SOAPMessage msg){
//        KalkanProvider kalkanProvider = new KalkanProvider();
//        Security.addProvider(kalkanProvider);
//        KncaXS.loadXMLSecurity();
//
//        try {
//            final String signMethod;
//            final String digestMethod;
////            InputStream is = new ByteArrayInputStream(SIMPLE_XML_SOAP.getBytes());
////            SOAPMessage msg = MessageFactory.newInstance().createMessage(null, is);
//            SOAPEnvelope env = msg.getSOAPPart().getEnvelope();
//            SOAPBody body = env.getBody();
//
//            String bodyId = "id-" + UUID.randomUUID().toString();
//            body.addAttribute(new QName(WSConstants.WSU_NS, "Id", WSConstants.WSU_PREFIX), bodyId);
//
//            SOAPHeader header = env.getHeader();
//            if (header == null) {
//                header = env.addHeader();
//            }
//            KeyStore store = KeyStore.getInstance("PKCS12", KalkanProvider.PROVIDER_NAME);
//            InputStream inputStream;
//            inputStream = AccessController.doPrivileged(new PrivilegedExceptionAction<InputStream>() {
//                @Override
//                public FileInputStream run() throws Exception {
//                    return new FileInputStream(keyStoreName);
//                }
//            });
//            store.load(inputStream, keyStorePassword.toCharArray());
//            Enumeration<String> als = store.aliases();
//            String alias = null;
//            while (als.hasMoreElements()) {
//                alias = als.nextElement();
//            }
//            final PrivateKey privateKey = (PrivateKey) store.getKey(alias, keyStorePassword.toCharArray());
//            final X509Certificate x509Certificate = (X509Certificate) store.getCertificate(alias);
//            String sigAlgOid = x509Certificate.getSigAlgOID();
//            if (sigAlgOid.equals(PKCSObjectIdentifiers.sha1WithRSAEncryption.getId())) {
//                signMethod = Constants.MoreAlgorithmsSpecNS + "rsa-sha1";
//                digestMethod = Constants.MoreAlgorithmsSpecNS + "sha1";
//            } else if (sigAlgOid.equals(PKCSObjectIdentifiers.sha256WithRSAEncryption.getId())) {
//                signMethod = Constants.MoreAlgorithmsSpecNS + "rsa-sha256";
//                digestMethod = XMLCipherParameters.SHA256;
//            } else {
//                signMethod = Constants.MoreAlgorithmsSpecNS + "gost34310-gost34311";
//                digestMethod = Constants.MoreAlgorithmsSpecNS + "gost34311";
//            }
//
//            Document doc = env.getOwnerDocument();
//            Transforms transforms = new Transforms(env.getOwnerDocument());
//
////      ###########################################################################################
//            transforms.addTransform(Transforms.TRANSFORM_C14N_EXCL_OMIT_COMMENTS);
//            Element c14nMethod = XMLUtils.createElementInSignatureSpace(doc, "CanonicalizationMethod");
//            c14nMethod.setAttributeNS(null, "Algorithm", Canonicalizer.ALGO_ID_C14N_EXCL_OMIT_COMMENTS);
//
//            Element signatureMethod = XMLUtils.createElementInSignatureSpace(doc, "SignatureMethod");
//            signatureMethod.setAttributeNS(null, "Algorithm", signMethod);
//
//            XMLSignature sig = new XMLSignature(env.getOwnerDocument(), "", signatureMethod, c14nMethod);
//
//            sig.addDocument("#" + bodyId, transforms, digestMethod);
//            sig.getSignedInfo().getSignatureMethodElement().setNodeValue(Transforms.TRANSFORM_C14N_EXCL_OMIT_COMMENTS);
//
//            WSSecHeader secHeader = new WSSecHeader();
//            secHeader.setMustUnderstand(true);
//            secHeader.insertSecurityHeader(env.getOwnerDocument());
//            secHeader.getSecurityHeader().appendChild(sig.getElement());
//            header.appendChild(secHeader.getSecurityHeader());
//
//            SecurityTokenReference reference = new SecurityTokenReference(doc);
//            reference.setKeyIdentifier(x509Certificate);
//
//            sig.getKeyInfo().addUnknownElement(reference.getElement());
//            sig.sign(privateKey);
//
//            return true;
//        } catch (Exception e) {
//            log.error("FAILED TO SIGN SOAP MESSAGE", e);
//            return false;
//        }
//    }
}
