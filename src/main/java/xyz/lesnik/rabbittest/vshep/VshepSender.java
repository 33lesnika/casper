package xyz.lesnik.rabbittest.vshep;

import xyz.lesnik.rabbittest.entity.RecognitionEvent;

/**
 * 25.02.2021
 * VshepSender
 * 05:57
 */
public interface VshepSender {
    String send(RecognitionEvent event, boolean cutLastCharacters) throws VshepSendingException;
}
