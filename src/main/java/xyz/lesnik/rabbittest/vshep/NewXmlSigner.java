package xyz.lesnik.rabbittest.vshep;

import kz.gov.pki.kalkan.asn1.pkcs.PKCSObjectIdentifiers;
import kz.gov.pki.kalkan.jce.provider.KalkanProvider;
import kz.gov.pki.kalkan.xmldsig.KncaXS;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.ws.security.WSConstants;
import org.apache.ws.security.message.WSSecHeader;
import org.apache.ws.security.message.token.SecurityTokenReference;
import org.apache.xml.security.c14n.Canonicalizer;
import org.apache.xml.security.encryption.XMLCipherParameters;
import org.apache.xml.security.signature.XMLSignature;
import org.apache.xml.security.transforms.Transforms;
import org.apache.xml.security.utils.Constants;
import org.apache.xml.security.utils.XMLUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.security.*;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.UUID;

/**
 * 12.03.2021
 * Helper
 * 10:40
 */
@Slf4j
public class NewXmlSigner {

    private final String keyStoreName;
    private final String keyStorePassword;

    public NewXmlSigner(String keyStoreName, String keyStorePassword) {
        this.keyStoreName = keyStoreName;
        this.keyStorePassword = keyStorePassword;
    }

//    public void sign() throws SOAPException {
//        //SOAPMessage msgPre это ваш вариант SOAPMessage не подписанный
//        SOAPMessage msgPre = soapWebService.createSOAPRequest(requestString, messageId, serviceId);
//        String xml = xmlFormatUtil.convertMessageToString(msgPre);
//        InputStream is = new ByteArrayInputStream(xml.getBytes());
//        SOAPMessage msg = MessageFactory.newInstance().createMessage(null, is);
//        log.info("rId: {}, ----------------------addHeadSecurity------------------------------------", rId);
//        boolean isSigned = signService.addHeadSecurity(msg);
//        SOAPMessage resultSOAPMsg = null;
//        if (!isSigned) {
//            result.put("status", "failed");
//            result.put("message", "failed to sign SoapMessage");
//        } else {
//            log.info("rId: {}, ----------------------soapWebService---------------------------------", rId);
//            resultSOAPMsg = callSoapWebService(rId, msg);
//        }
//    }

//        -----xmlFormatUtil.convertMessageToString

    public String convertMessageToString(SOAPMessage message) {
        try {
            ByteArrayOutputStream byteOutput = new java.io.ByteArrayOutputStream();
            Result result = new StreamResult(byteOutput);
            Source source = new DOMSource(message.getSOAPPart());
            // write the DOM document to the file
            Transformer transformer;
            transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "YES");
            transformer.transform(source, result);
            String resultText = byteOutput.toString();

            // вот это тоже иногда нужно было для данных которые были в CDATA, вам возможно он и не нужен будет
            resultText = resultText.replaceAll("&gt;", ">").replaceAll("&lt;", "<").replaceAll("&#13;", "");

            return resultText;
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
        return null;
    }

    @SneakyThrows
    public String xmlToString(Document doc) {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformer.setOutputProperty(OutputKeys.INDENT, "no");
        StringWriter writer = new StringWriter();
        transformer.transform(new DOMSource(doc), new StreamResult(writer));
        var s = writer.toString();
        BufferedReader br = new BufferedReader(new StringReader(s));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            line = line.trim();
            if (line.length() != 0) {
                sb.append(line);
            }
        }
        return sb.toString();
    }
//        -----signService.addHeadSecurity

    public boolean addHeadSecurity(SOAPMessage msg) {
        String KEYSTORE_PASSWORD = keyStorePassword;
        String KEYSTORE_KEY = keyStoreName;
        KalkanProvider kalkanProvider = new KalkanProvider();
        Security.addProvider(kalkanProvider);
        KncaXS.loadXMLSecurity();

        try {
            final String signMethod;
            final String digestMethod;
//            InputStream is = new ByteArrayInputStream(SIMPLE_XML_SOAP.getBytes());
//            SOAPMessage msg = MessageFactory.newInstance().createMessage(null, is);
            SOAPEnvelope env = msg.getSOAPPart().getEnvelope();
            SOAPBody body = env.getBody();

            String bodyId = "id-" + UUID.randomUUID().toString();
            body.addAttribute(new QName(WSConstants.WSU_NS, "Id", WSConstants.WSU_PREFIX), bodyId);

            SOAPHeader header = env.getHeader();
            if (header == null) {
                header = env.addHeader();
            }
            KeyStore store = KeyStore.getInstance("PKCS12", KalkanProvider.PROVIDER_NAME);
            InputStream inputStream;
            inputStream = AccessController.doPrivileged(new PrivilegedExceptionAction<InputStream>() {
                @Override
                public FileInputStream run() throws Exception {
                    return new FileInputStream(KEYSTORE_KEY);
                }
            });
            store.load(inputStream, KEYSTORE_PASSWORD.toCharArray());
            Enumeration<String> als = store.aliases();
            String alias = null;
            while (als.hasMoreElements()) {
                alias = als.nextElement();
            }
            final PrivateKey privateKey = (PrivateKey) store.getKey(alias, KEYSTORE_PASSWORD.toCharArray());
            final X509Certificate x509Certificate = (X509Certificate) store.getCertificate(alias);
            String sigAlgOid = x509Certificate.getSigAlgOID();
            if (sigAlgOid.equals(PKCSObjectIdentifiers.sha1WithRSAEncryption.getId())) {
                signMethod = Constants.MoreAlgorithmsSpecNS + "rsa-sha1";
                digestMethod = Constants.MoreAlgorithmsSpecNS + "sha1";
            } else if (sigAlgOid.equals(PKCSObjectIdentifiers.sha256WithRSAEncryption.getId())) {
                signMethod = Constants.MoreAlgorithmsSpecNS + "rsa-sha256";
                digestMethod = XMLCipherParameters.SHA256;
            } else {
                signMethod = Constants.MoreAlgorithmsSpecNS + "gost34310-gost34311";
                digestMethod = Constants.MoreAlgorithmsSpecNS + "gost34311";
            }

            Document doc = env.getOwnerDocument();
            Transforms transforms = new Transforms(env.getOwnerDocument());

//      ###########################################################################################
            transforms.addTransform(Transforms.TRANSFORM_C14N_EXCL_OMIT_COMMENTS);
            Element c14nMethod = XMLUtils.createElementInSignatureSpace(doc, "CanonicalizationMethod");
            c14nMethod.setAttributeNS(null, "Algorithm", Canonicalizer.ALGO_ID_C14N_EXCL_OMIT_COMMENTS);

            Element signatureMethod = XMLUtils.createElementInSignatureSpace(doc, "SignatureMethod");
            signatureMethod.setAttributeNS(null, "Algorithm", signMethod);

            XMLSignature sig = new XMLSignature(env.getOwnerDocument(), "", signatureMethod, c14nMethod);

            sig.addDocument("#" + bodyId, transforms, digestMethod);
            sig.getSignedInfo().getSignatureMethodElement().setNodeValue(Transforms.TRANSFORM_C14N_EXCL_OMIT_COMMENTS);

            WSSecHeader secHeader = new WSSecHeader();
            secHeader.setMustUnderstand(true);
            secHeader.insertSecurityHeader(env.getOwnerDocument());
            secHeader.getSecurityHeader().appendChild(sig.getElement());
            header.appendChild(secHeader.getSecurityHeader());

            SecurityTokenReference reference = new SecurityTokenReference(doc);
            reference.setKeyIdentifier(x509Certificate);

            sig.getKeyInfo().addUnknownElement(reference.getElement());
            sig.sign(privateKey);

            return true;
        } catch (Exception e) {
            log.error("FAILED TO SIGN SOAP MESSAGE", e);
            return false;
        }
    }


//        ----soapWebService.callSoapWebService

//        public SOAPMessage callSoapWebService (String rId, SOAPMessage msg){
//            try {
//                String soapEndpointUrl = applicationProperties.getVshepService().getUrl() + "/bip-sync-wss-gost/";
//                log.info("rId: {}, url: {}", rId, soapEndpointUrl);
//                // Create SOAP Connection
//                SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
//                SOAPConnection soapConnection = soapConnectionFactory.createConnection();
//
//                // Send SOAP Message to SOAP Server
//                SOAPMessage soapResponse = soapConnection.call(msg, soapEndpointUrl);
//
//                ByteArrayOutputStream out = new ByteArrayOutputStream();
//                soapResponse.writeTo(out);
//
//                InputStream is = new ByteArrayInputStream(out.toByteArray());
//                SOAPMessage resultMsg = MessageFactory.newInstance().createMessage(null, is);
//                soapConnection.close();
//
//                return resultMsg;
//            } catch (Exception e) {
//                e.printStackTrace();
//                log.error("rId: {}, Error occurred while sending SOAP Request to Server [НЕДОСТУПЕН ВШЭП]!", rId);
//                return null;
//            }
//        }

}
