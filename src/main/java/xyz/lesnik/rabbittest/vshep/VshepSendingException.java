package xyz.lesnik.rabbittest.vshep;

/**
 * 25.02.2021
 * VshepSendingException
 * 06:31
 */
public class VshepSendingException extends Exception{
    public VshepSendingException() {
    }

    public VshepSendingException(String message) {
        super(message);
    }

    public VshepSendingException(String message, Throwable cause) {
        super(message, cause);
    }

    public VshepSendingException(Throwable cause) {
        super(cause);
    }
}
