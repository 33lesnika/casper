package xyz.lesnik.rabbittest.vshep;

import java.util.Map;

/**
 * 25.02.2021
 * TemplateXmlView
 * 06:07
 */

public class TemplateXmlView {

    private final String template;

    public TemplateXmlView(String template) {
        this.template = template;
    }

    public String renderWith(Map<String, String> modelMap) {
        if (modelMap == null || modelMap.isEmpty()){
            return template;
        }
        String copy = template;
        for (Map.Entry<String, String> entry : modelMap.entrySet()) {
            String k = entry.getKey();
            String v = entry.getValue();
            if (v == null) {
                v = "";
            }
            copy = copy.replaceAll("\\{\\{" + k + "\\}\\}", v);
        }
        return copy;
    }
}
