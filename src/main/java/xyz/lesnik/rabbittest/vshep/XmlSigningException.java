package xyz.lesnik.rabbittest.vshep;

/**
 * 25.02.2021
 * XmlSigningException
 * 06:28
 */
public class XmlSigningException extends Exception{
    public XmlSigningException() {
    }

    public XmlSigningException(String message) {
        super(message);
    }

    public XmlSigningException(String message, Throwable cause) {
        super(message, cause);
    }

    public XmlSigningException(Throwable cause) {
        super(cause);
    }
}
