package xyz.lesnik.rabbittest.vshep;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.mongodb.core.aggregation.AccumulatorOperators;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import xyz.lesnik.rabbittest.entity.*;
import xyz.lesnik.rabbittest.event.SendToVshepEvent;
import xyz.lesnik.rabbittest.service.ImageService;
import xyz.lesnik.rabbittest.soap.ShaftInfo;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;

/**
 * 25.02.2021
 * SoapVshepSender
 * 05:58
 */
@Service
@Profile("vshep")
@Slf4j
public class SoapVshepSender implements VshepSender {

    private final TemplateXmlView templateXmlView;
    private final NewXmlSigner xmlSigner;
    private final SoapVshepConfig config;
    private final TaskExecutor applicationExecutor;
    private final ImageService imageService;
    private static final int MAX_RETRY_COUNT = 3;

    public SoapVshepSender(TemplateXmlView templateXmlView, NewXmlSigner xmlSigner, SoapVshepConfig config, TaskExecutor applicationExecutor, ImageService imageService) {
        this.templateXmlView = templateXmlView;
        this.xmlSigner = xmlSigner;
        this.config = config;
        this.applicationExecutor = applicationExecutor;
        this.imageService = imageService;
    }

    @Override
    public String send(RecognitionEvent event, boolean cutLastCharacters) throws VshepSendingException {
        Map<String, String> model = toModel(event, cutLastCharacters);
        String envelope = templateXmlView.renderWith(model);
        String signedXml = null;
        try {
            InputStream is = new ByteArrayInputStream(envelope.getBytes());
            SOAPMessage msg = MessageFactory.newInstance().createMessage(null, is);
//            xmlSigner.addHeadSecurity(msg);
            signedXml = xmlSigner.xmlToString(msg.getSOAPPart());
            if (log.isTraceEnabled()) {
                System.out.println(signedXml);
            }
        } catch (SOAPException | IOException e) {
            e.printStackTrace();
            throw new VshepSendingException(e);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_XML);
        var xmlHttpEntity = new HttpEntity<>(signedXml, headers);
        String errorMessage = null;
        for (int i = 0; i < MAX_RETRY_COUNT; i++) {
            log.info("Try ({}) send event ID: {}", i, event.getId());
            try {
                var stringResponseEntity = new RestTemplate().postForEntity(config.getUrl(), xmlHttpEntity, String.class);
                if (stringResponseEntity.getStatusCode().is2xxSuccessful()) {
                    return stringResponseEntity.getBody();
                } else {
                    errorMessage = "Got error from VSHEP, code: " + stringResponseEntity.getStatusCodeValue()
                            + "; response: " + stringResponseEntity.getBody() + "; event ID: " + event.getId();
                    log.error(errorMessage);
                }
            } catch (Exception ex) {
                log.error("Got error from VSHEP: {} for event ID: {}", ex.getMessage(), event.getId());
                errorMessage = "Got error from VSHEP, message: " + ex.getMessage() + "; event ID: " + event.getId();
            }
        }
        throw new VshepSendingException(errorMessage);
    }


    DecimalFormat df = new DecimalFormat("#.00",
            DecimalFormatSymbols.getInstance(Locale.US));

    public Map<String, String> toModel(RecognitionEvent event, boolean cutLastCharacters) {
        Map<String, String> model = new HashMap<>();
        if (event.getViolations() == null) {
            event.setViolations(new HashSet<>());
        }
        model.put("message.id", event.getId() != null ? event.getId().toString() : UUID.randomUUID().toString());
        model.put("message.date", ZonedDateTime.now().toString().replaceAll("\\[.*]", ""));
        String archCode = event.getName();
        if (cutLastCharacters) {
            archCode = event.getName().length() < 3 ?
                    event.getName() :
                    event.getName().substring(0, event.getName().length() - 3);
        }
        model.put("arch.code", archCode);
        model.put("createdBy", "06010102-3000-4250-0000-000000000011");
        model.put("software.version", "2.3.1.0");
        model.put("credence", "0");
        model.put("way.type", String.valueOf(event.getDirection()));
        model.put("direction", String.valueOf(event.getDirection() + 1));
        model.put("lane", "1");
        model.put("recordDate", ZonedDateTime.ofInstant(Instant.ofEpochMilli(event.getTimestamp()), ZoneId.systemDefault()).toString().replaceAll("\\[.*]", ""));
        model.put("vehicle.number", event.getGrnz());
        model.put("vehicle.country", "KAZ");
        model.put("vehicle.numberRear", event.getGrnz());
        model.put("hasVehicleNumberPhoto", "false");
        if (event.getPlatefrontId() != null) {
            model.put("hasVehicleNumberPhoto", "true");
        }
        model.put("vehicle.type", String.valueOf(event.getClassWIM()));

        model.put("uncorrectType", "0");
        model.put("speed", df.format(event.getVelocity()));
        model.put("length", String.valueOf((int) (event.getVehicleLength() * 100)));
        model.put("width", String.valueOf((int) (event.getVehicleWidth() * 100)));
        model.put("height", String.valueOf((int) (event.getVehicleHeight() * 100)));
        model.put("wheelBase", String.valueOf((int) (event.getWheelBase() * 100)));
        model.put("weight", String.valueOf(event.getGrossWeight()));
        boolean isOverweight = event.getViolations() != null && event.getViolations().stream().anyMatch(x -> x.getType() == ViolationType.WEIGHT_PER_AXIS);
        if (isOverweight) {
            final double overweight = event.getViolations().stream()
                    .filter(x -> x.getType() == ViolationType.WEIGHT_PER_AXIS)
                    .mapToDouble(Violation::getValue)
                    .max().orElse(0d);
            model.put("overWeight", String.valueOf(overweight));
        } else {
            model.put("overWeight", "");
        }
        model.put("isOverweightGross", String.valueOf(isOverweight));
        model.put("isOverweightPartial", "false");
        model.put("isOverweight", String.valueOf(isOverweight));
        var isExceededWidth = false;
        if(event.getViolations() != null && event.getViolations().stream().anyMatch(x -> x.getType() == ViolationType.WIDTH)){
            isExceededWidth = true;
        }
        model.put("isExceededWidth", String.valueOf(isExceededWidth));
        var isExceededHeight = false;
        model.put("isExceededHeight", String.valueOf(isExceededHeight));
        var isExceededLength = false;
        if(event.getViolations() != null && event.getViolations().stream().anyMatch(x -> x.getType() == ViolationType.LENGTH)){
            isExceededLength = true;
        }
        model.put("isExceededLength", String.valueOf(isExceededLength));
        var isOversized = isExceededHeight || isExceededLength || isExceededWidth;
        model.put("isOversized", String.valueOf(isOversized));
        model.put("isWrongDirection", "false");
        model.put("isNonStandard", "false");
        model.put("isExceededOperatingRange", "false");
        model.put("countryCode", "KAZ");
        model.put("signature", "");

        List<String> photos = getPhotosString(event.getAnprfrontId(), event.getOvcfrontId(),
                event.getPlatefrontId(), event.getAnprrearId(), event.getPlaterearId());
        if (!photos.isEmpty()) {
            model.put("photos", String.join("\n", photos));
        } else {
            model.put("photos", "");
        }
        if (event.getAxles() != null && !event.getAxles().isEmpty()) {
            model.put("shafts", String.join("\n", addShafts(event.getAxles())));
        } else {
            model.put("shafts", "");
        }
        return model;
    }

    private List<String> getPhotosString(UUID... uuids) {
        List<String> result = new ArrayList<>();
        for (UUID uuid : uuids) {
            if (uuid == null) continue;
            try {
                final Image image = imageService.get(uuid);
                StringBuilder sb = new StringBuilder();
                sb.append("<photo><data>");
                sb.append(new String(Base64.getEncoder().encode(image.getData())));
                sb.append("</data></photo>");
                result.add(sb.toString());
            } catch (Exception e) {
                log.error("Cannot add photo to xml, reason: {}", e.getMessage());
            }
        }
        return result;
    }

    private List<String> addShafts(List<Axle> axles) {
        List<String> result = new ArrayList<>();
        if (axles == null) return Collections.emptyList();
        for (Axle axle : axles) {
            StringBuilder sb = new StringBuilder();
            sb.append("<shaftInfo>");
            sb.append("<number>").append(axle.getNumber()).append("</number>");
            sb.append("<groupNumber>").append(1).append("</groupNumber>");
            sb.append("<weight>").append(axle.getWeight()).append("</weight>");
            sb.append("<distance>").append(axle.getDistance()).append("</distance>");
            sb.append("</shaftInfo>");
            result.add(sb.toString());
        }
        return result;
    }

    private ShaftInfo toShaftInfo(Axle axle) {
        ShaftInfo shaft = new ShaftInfo();
        shaft.setNumber(axle.getNumber());
        shaft.setDistance(axle.getDistance());
        shaft.setWeight(axle.getWeight());
        shaft.setGroupNumber(1);
        return shaft;
    }

    @EventListener
    public void onViolationDetectedEvent(SendToVshepEvent event) {
        applicationExecutor.execute(() -> {
            if (log.isDebugEnabled()) {
                log.debug("Sending event to VSHEP: {}", event);
            }
            try {
                final String response = send(event.getEvent(), event.isCutLastCharacters());
                event.getEvent().setSendStatus(SendStatus.SUCCESS);
                if (log.isDebugEnabled()) {
                    log.debug("VSHEP response: {}, event timestamp: {}", response, event.getTimestamp());
                }
            } catch (VshepSendingException e) {
                event.getEvent().setSendStatus(SendStatus.FAILURE);
                log.error("Cannot send event to VSHEP because of: {}; event: {}", e.getMessage(), event);
            }
            if (event.getCallback() != null) {
                event.getCallback().accept(event.getEvent());
            }
        });
    }

}
