package xyz.lesnik.rabbittest.vshep;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import xyz.lesnik.rabbittest.entity.RecognitionEvent;

/**
 * 25.02.2021
 * NoopVshepSender
 * 06:00
 */
@Service
@Profile("!vshep")
public class NoopVshepSender implements VshepSender{
    @Override
    public String send(RecognitionEvent event, boolean cutLastCharacters) {
        return null;
    }
}
