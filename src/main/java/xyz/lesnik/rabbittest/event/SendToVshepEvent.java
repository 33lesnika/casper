package xyz.lesnik.rabbittest.event;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.springframework.context.ApplicationEvent;
import xyz.lesnik.rabbittest.entity.RecognitionEvent;

import java.util.function.Consumer;

/**
 * 03.09.2021
 * SendToVshepEvent
 * 22:26
 */
@Getter
@ToString
@EqualsAndHashCode
public class SendToVshepEvent extends ApplicationEvent {

    private final RecognitionEvent event;
    private final boolean cutLastCharacters;
    private final Consumer<RecognitionEvent> callback;

    public SendToVshepEvent(Object source, RecognitionEvent event, boolean cutLastCharacters) {
        super(source);
        this.event = event;
        this.cutLastCharacters = cutLastCharacters;
        this.callback = null;
    }


    public SendToVshepEvent(Object source, RecognitionEvent event, boolean cutLastCharacters, Consumer<RecognitionEvent> callback) {
        super(source);
        this.event = event;
        this.cutLastCharacters = cutLastCharacters;
        this.callback = callback;
    }
}
