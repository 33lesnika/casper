package xyz.lesnik.rabbittest.runner;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import xyz.lesnik.rabbittest.security.authentication.UserAccount;
import xyz.lesnik.rabbittest.security.authentication.UserAccountRepository;

/**
 * 22.12.2020
 * EnsureAdminExistsRunner
 * 17:01
 */
@Component
@Slf4j
public class EnsureAdminExistsRunner implements CommandLineRunner {

    private final UserAccountRepository repository;
    private final PasswordEncoder encoder;

    public EnsureAdminExistsRunner(UserAccountRepository repository, PasswordEncoder encoder) {
        this.repository = repository;
        this.encoder = encoder;
    }

    @Override
    public void run(String... args) throws Exception {
        UserAccount admin = repository.findByUsername("admin").orElseGet(() -> {
            log.info("Creating default 'admin' account");
            UserAccount userAccount = new UserAccount();
            userAccount.setUsername("admin");
            userAccount.setPassword(encoder.encode("admin"));
            userAccount.setAuth("");
            userAccount.setFio("Администратор");
            userAccount.setAccountNonExpired(true);
            userAccount.setEnabled(true);
            userAccount.setAccountNonLocked(true);
            userAccount.setCredentialsNonExpired(true);
            return userAccount;
        });
        repository.save(admin);
    }
}
