package xyz.lesnik.rabbittest.runner;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import xyz.lesnik.rabbittest.entity.CarType;
import xyz.lesnik.rabbittest.service.CarTypeService;

import java.util.ArrayList;
import java.util.List;

/**
 * 31.01.2021
 * FillCarTypesIfNotExists
 * 13:09
 */
@Component
@Slf4j
public class FillCarTypesIfNotExists implements CommandLineRunner {

    private final CarTypeService carTypeService;

    public FillCarTypesIfNotExists(CarTypeService carTypeService) {
        this.carTypeService = carTypeService;
    }

    @Override
    public void run(String... args) throws Exception {
        if (carTypeService.getAll().isEmpty()){
            log.info("No car types detected in database, creating default car types");
            var iterator = carTypeService.saveAll(defaultCarTypes());
            List<CarType> carTypeList = new ArrayList<>();
            iterator.forEach(carTypeList::add);
            log.info("Added {} car types into database", carTypeList.size());
        }
    }

    private List<CarType> defaultCarTypes() {
        return List.of(
                new CarType(1, "Двухосный автомобиль или автобус", 2, 1, 18000.0, 4.0, 12.0, 2.55),
                new CarType(2, "Трёхосный автомобиль", 2, 3, 25_000, 4.0, 12.0, 2.55),
                new CarType(3, "Трёхосный автомобиль, имеющий ведущую ось, состоящую из двух пар колёс, оборудованных воздушной или эквивалентной ей подвеской", 3, 3, 26_000, 4.0, 12.0, 2.55),
                new CarType(4, "Трёхосный шарнирно сочленённый автобус", 3, 12, 28_000, 4.0, 44_329, 2.55),
                new CarType(5, "Четырёхосный автомобиль с двумя ведущими осями, каждая из которых состоит из двух пар колёс и имеет воздушную или эквивалентную ей подвеску", 4, 4, 32000.0, 4.0, 12.0, 2.55),
                new CarType(6, "Двухосный тягач с двухосным полуприцепом", 4, 8, 36000.0, 4.0, 20.0, 2.55),
                new CarType(7, "Двухосный грузовой автомобиль с двухосным прицепом", 4, 5, 36000.0, 4.0, 20.0, 2.55),
                new CarType(8, "Четырёхосный шарнирно сочленённый автобус", 4, 12, 32000.0, 4.0, 18.75, 2.55),
                new CarType(9, "Двухосный тягач с трёхосным полуприцепом", 5, 9, 40000.0, 4.0, 20.0, 2.55),
                new CarType(10, "Трёхосный тягач с двухосным полуприцепом", 5, 10, 40000.0, 4.0, 20.0, 2.55),
                new CarType(11, "Двухосный грузовой автомобиль с трёхосным прицепом", 5, 5, 42000.0, 4.0, 20.0, 2.55),
                new CarType(12, "Трёхосный грузовой автомобиль с двухосным прицепом", 5, 6, 44000.0, 4.0, 20.0, 2.55),
                new CarType(13, "Трёхосный грузовой автомобиль с трёхосным прицепом", 6, 6, 44000.0, 4.0, 20.0, 2.55),
                new CarType(14, "Трёхосный тягач с трёхосным полуприцепом", 6, 11, 40000.0, 4.0, 20.0, 2.55),
                new CarType(15, "Трёхосный тягач с четырёхосным полуприцепом", 7, 13, 44000.0, 4.0, 20.0, 2.55)
        );
    }
}
