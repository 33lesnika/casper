package xyz.lesnik.rabbittest.runner;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import xyz.lesnik.rabbittest.entity.RecognitionEvent;
import xyz.lesnik.rabbittest.vshep.VshepSender;

/**
 * 25.02.2021
 * VshepTestSender
 * 07:55
 */
@Component
@Profile("vshep & vsheptest")
@Slf4j
public class VshepTestSender implements CommandLineRunner, ApplicationContextAware {

    private ApplicationContext applicationContext;

    String eventJson = "{\n" +
            "  \"id\": \"b5af8d25-2b54-4da0-896d-333ad249c81b\",\n" +
            "  \"name\": \"A01B54R1P02L01\",\n" +
            "  \"alias\": \"Нур-Султан-Кабанбай Батыра-Темиртау, км 22\",\n" +
            "  \"timestamp\": 1614168855118,\n" +
            "  \"quality\": 52,\n" +
            "  \"anprfrontId\": \"2edf885b-88d8-356a-95c8-e7a68e3f94dd\",\n" +
            "  \"ovcfrontId\": \"fabb488a-8203-3c9b-b8f2-b2da97e4e57d\",\n" +
            "  \"platefrontId\": \"b2033fa3-637f-397f-9cd3-2eabbaf9f5a2\",\n" +
            "  \"anprrearId\": \"a6960b06-9bb0-3ec1-a18c-9a251b711567\",\n" +
            "  \"platerearId\": \"2391d822-c771-374d-a386-77efd5e911ae\",\n" +
            "  \"velocity\": 77.5,\n" +
            "  \"direction\": 0,\n" +
            "  \"violations\": [\n" +
            "    {\n" +
            "      \"type\": \"TYPE_NOT_FOUND\",\n" +
            "      \"description\": \"Тип ТС не определён\",\n" +
            "      \"details\": \"Нет данных по типу ТС с количестом осей 2, класс 12\"\n" +
            "    }\n" +
            "  ],\n" +
            "  \"carType\": null,\n" +
            "  \"history\": [],\n" +
            "  \"GRNZ\": \"411CDZ01\",\n" +
            "  \"axlescount\": 2,\n" +
            "  \"errorflag\": \"0\",\n" +
            "  \"warningflag\": 32,\n" +
            "  \"grossweight\": 4320,\n" +
            "  \"classwim\": 12,\n" +
            "  \"classsick\": null,\n" +
            "  \"classKZ\": null,\n" +
            "  \"wheelbase\": 4.34,\n" +
            "  \"vehiclelength\": 0,\n" +
            "  \"vehicleheight\": 0,\n" +
            "  \"vehiclewidth\": 0\n" +
            "}";

    private final VshepSender vshepSender;
    private final ObjectMapper mapper;

    public VshepTestSender(VshepSender vshepSender, ObjectMapper mapper) {
        this.vshepSender = vshepSender;
        this.mapper = mapper;
    }


    @Override
    public void run(String... args) throws Exception {
        RecognitionEvent event = mapper.readValue(eventJson, RecognitionEvent.class);
        log.info("Sending to VSHEP: {}", event);
        var send = vshepSender.send(event, false);
        log.info("Received response from VHSEP: {}", send);
        SpringApplication.exit(applicationContext);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
