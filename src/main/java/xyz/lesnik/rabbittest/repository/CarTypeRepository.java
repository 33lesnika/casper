package xyz.lesnik.rabbittest.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import xyz.lesnik.rabbittest.entity.CarType;

import java.util.List;

/**
 * 31.01.2021
 * CarTypeRepository
 * 12:56
 */
@Repository
public interface CarTypeRepository extends CrudRepository<CarType, Integer> {
    List<CarType> findAll();
}
