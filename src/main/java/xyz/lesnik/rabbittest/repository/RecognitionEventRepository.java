package xyz.lesnik.rabbittest.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import xyz.lesnik.rabbittest.controller.dto.ArchStatistics;
import xyz.lesnik.rabbittest.controller.dto.PercentageInfo;
import xyz.lesnik.rabbittest.controller.dto.SummaryCountsInterface;
import xyz.lesnik.rabbittest.entity.RecognitionEvent;
import xyz.lesnik.rabbittest.entity.SendStatus;
import xyz.lesnik.rabbittest.model.SendStatusCount;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

/**
 * 14.12.2020
 * RecognitionEventRepository
 * 21:41
 */

@Repository
public interface RecognitionEventRepository extends JpaRepository<RecognitionEvent, UUID> {
    Page<RecognitionEvent> findAllByViolationsIsNotNullOrderByTimestampDesc(Pageable pageable);

    Page<RecognitionEvent> findAllByOrderByTimestampDesc(Pageable pageable);

    Page<RecognitionEvent> findAllByTimestampBetween(Long from, Long to, Pageable pageable);

    Stream<RecognitionEvent> findAllByTimestampBetween(Long from, Long to);

    List<RecognitionEvent> findAllBySendStatus(SendStatus sendStatus);

    @Query(value = "select alias as arch, " +
            "count(*) as allCount, " +
            "sum(case when violations @> '[{\"type\":\"WEIGHT_PER_AXIS\"}]' then 1 else 0 end) as overweightCount, " +
            "sum(case when violations is null then 1 else 0 end) as noViolationsCount, " +
            "sum(case when send_status = 'SUCCESS' then 1 else 0 end) as sentCount " +
            "from recognition_event where timestamp between :start and :end group by alias",
            nativeQuery = true)
    List<SummaryCountsInterface> generateStatistics(@Param("start") long start, @Param("end") long end);

    @Query(value = "select send_status as send_status, count(*) as count from recognition_event group by send_status", nativeQuery = true)
    List<SendStatusCount> generateStatistics();

    @Query(value = "select " +
            "count(*) as allCount, " +
            "sum(case when violations is null then 1 else 0 end) as noViolationsCount " +
            "from recognition_event where timestamp between :start and :end",
            nativeQuery = true)
    PercentageInfo generatePercentageReport(@Param("start") long start, @Param("end") long end);

    @Query(value = "select name as name, " +
            "count(*) as allCount, " +
            "sum(case when violations is null then 1 else 0 end) as noViolationsCount " +
            "from recognition_event where timestamp between :start and :end group by name",
            nativeQuery = true)
    List<ArchStatistics> generateViolationsReportByArchCode(@Param("start") long start, @Param("end") long end);
}
