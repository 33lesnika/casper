package xyz.lesnik.rabbittest.repository;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;
import xyz.lesnik.rabbittest.entity.Image;

import java.util.Optional;
import java.util.UUID;

/**
 * 14.12.2020
 * ImageRepositoryImpl
 * 22:31
 */

@Repository
public class ImageRepositoryImpl implements ImageRepository {
    private final MongoTemplate mongoTemplate;

    public ImageRepositoryImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public Optional<Image> get(UUID uuid) {
        return Optional.ofNullable(mongoTemplate.findById(uuid, Image.class));
    }

    public Iterable<Image> getAll() {
        return mongoTemplate.findAll(Image.class);
    }

    @Override
    public Image create(Image image) {
        return mongoTemplate.save(image);
    }

    @Override
    public void delete(Image image) {
        mongoTemplate.remove(image);
    }
}
