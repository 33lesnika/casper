package xyz.lesnik.rabbittest.repository;

import xyz.lesnik.rabbittest.entity.Image;

import java.util.Optional;
import java.util.UUID;

/**
 * 14.12.2020
 * ImageRepository
 * 22:09
 */
public interface ImageRepository {
    Optional<Image> get(UUID uuid);
    Iterable<Image> getAll();
    Image create(Image image);
    void delete(Image image);
}
