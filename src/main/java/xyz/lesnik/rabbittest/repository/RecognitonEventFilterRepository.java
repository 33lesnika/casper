package xyz.lesnik.rabbittest.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import xyz.lesnik.rabbittest.controller.dto.FilterParams;
import xyz.lesnik.rabbittest.controller.dto.SortOrder;
import xyz.lesnik.rabbittest.entity.RecognitionEvent;
import xyz.lesnik.rabbittest.entity.ViolationType;
import xyz.lesnik.rabbittest.util.FixedTotalCountPage;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * 27.04.2021
 * RecoginitonEventFilterRepository
 * 00:53
 */
@Repository
public class RecognitonEventFilterRepository {

    private final EntityManager em;

    private static final long PAGE_SIZE = 3000L;

    public RecognitonEventFilterRepository(EntityManager em) {
        this.em = em;
    }

    public Page<RecognitionEvent> getEvents(FilterParams params, Pageable pageable) {
//        weightMin=1&weightMax=5&widthMin=1&widthMax=6&lengthMin=2&lengthMax=6&=1&axlesMax=5&from=1617213600000&to=1617213600000&page=1&size=50
//        weightMin=1&weightMax=2000&axlesMin=1&axlesMax=3&carType=1&violationType=TYPE_NOT_FOUND&alias=%D0%90%D1%82%D0%B1%D0%B0%D1%81%D0%B0%D1%80&from=1617213600000&to=1619459999999&page=1&size=50
        String sql = "SELECT * from recognition_event re WHERE 1=1 ";

        if (StringUtils.hasText(params.getGrnz())) {
            sql += " AND (grnz = :grnz OR corrected_grnz = :grnz) ";
        }

        if (params.getWeightMin() != null) {
            sql += " AND gross_weight >= :weightMin";
        }
        if (params.getWeightMax() != null) {
            sql += " AND gross_weight <= :weightMax";
        }
        if (params.getWidthMin() != null) {
            sql += " AND vehicle_width >= :widthMin";
        }
        if (params.getWidthMax() != null) {
            sql += " AND vehicle_width <= :widthMax";
        }
        if (params.getLengthMin() != null) {
            sql += " AND vehicle_length >= :lengthMin";
        }
        if (params.getLengthMax() != null) {
            sql += " AND vehicle_length <= :lengthMax";
        }
        if (params.getAxlesMin() != null) {
            sql += " AND axles_count >= :axlesMin";
        }
        if (params.getAxlesMax() != null) {
            sql += " AND axles_count <= :axlesMax";
        }
        if (params.getCarType() != null) {
            sql += " AND car_type ->> 'id' = '" + params.getCarType() + "'";
        }
        if (params.getViolationType() != null) {
            sql += " and (violations @> '[{\"type\":\"" + params.getViolationType() + "\"}]'";
            if (params.getViolationType() == ViolationType.NO_VIOLATION) {
                sql += " or violations is null)";
            }
        }
        if (params.getAlias() != null) {
            sql += " AND alias like '%" + params.getAlias() + "%'";
        }
        if (params.getSendStatus() != null) {
            sql += " AND send_status = :sendStatus";
        }
        long from = params.getFrom() == null ? 0L : params.getFrom();
        long to = params.getTo() == null ? Long.MAX_VALUE : params.getTo();
        sql += " AND timestamp between :from and :to";
        String mainSql = sql;
        if (params.getSort() == null) {
            params.setSort(SortOrder.DESC);
        }
        sql += " ORDER BY timestamp " + params.getSort().name();
        if (pageable.isPaged()) {
            sql += " LIMIT :limit OFFSET :offset";
        }
        var query = em.createNativeQuery(sql, RecognitionEvent.class);
        setQueryParams(params, pageable, from, to, query);

        List<RecognitionEvent> result = (List<RecognitionEvent>) query.getResultList();
        // This query fetches the RecognitionEvent as per the Page Limit
        // Create Count Query
        var countQuery = em.createNativeQuery(mainSql.replaceFirst("SELECT \\* from", "SELECT count(*) from"));

        setQueryParams(params, from, to, countQuery);
        long count = ((BigInteger) countQuery.getSingleResult()).longValue();

        return new FixedTotalCountPage<>(result, pageable, count);
    }

    public Stream<RecognitionEvent> getEvents(FilterParams params) {
//        weightMin=1&weightMax=5&widthMin=1&widthMax=6&lengthMin=2&lengthMax=6&=1&axlesMax=5&from=1617213600000&to=1617213600000&page=1&size=50
//        weightMin=1&weightMax=2000&axlesMin=1&axlesMax=3&carType=1&violationType=TYPE_NOT_FOUND&alias=%D0%90%D1%82%D0%B1%D0%B0%D1%81%D0%B0%D1%80&from=1617213600000&to=1619459999999&page=1&size=50
        String sql = "SELECT * from recognition_event re WHERE 1=1 ";

        if (params.getWeightMin() != null) {
            sql += " AND gross_weight >= :weightMin";
        }
        if (params.getWeightMax() != null) {
            sql += " AND gross_weight <= :weightMax";
        }
        if (params.getWidthMin() != null) {
            sql += " AND vehicle_width >= :widthMin";
        }
        if (params.getWidthMax() != null) {
            sql += " AND vehicle_width <= :widthMax";
        }
        if (params.getLengthMin() != null) {
            sql += " AND vehicle_length >= :lengthMin";
        }
        if (params.getLengthMax() != null) {
            sql += " AND vehicle_length <= :lengthMax";
        }
        if (params.getAxlesMin() != null) {
            sql += " AND axles_count >= :axlesMin";
        }
        if (params.getAxlesMax() != null) {
            sql += " AND axles_count <= :axlesMax";
        }
        if (params.getCarType() != null) {
            sql += " AND car_type ->> 'id' = '" + params.getCarType() + "'";
        }
        if (params.getViolationType() != null) {
            sql += " and (violations @> '[{\"type\":\"" + params.getViolationType() + "\"}]'";
            if (params.getViolationType() == ViolationType.NO_VIOLATION) {
                sql += " or violations is null)";
            }
        }
        if (params.getAlias() != null) {
            sql += " AND alias like '%" + params.getAlias() + "%'";
        }

        long from = params.getFrom() == null ? 0L : params.getFrom();
        long to = params.getTo() == null ? Long.MAX_VALUE : params.getTo();
        sql += " AND timestamp between :from and :to";


        var countQuery = em.createNativeQuery(sql.replaceFirst("SELECT \\* from", "SELECT count(*) from"));

        setQueryParams(params, from, to, countQuery);
        long count = ((BigInteger) countQuery.getSingleResult()).longValue();
        long pageCount = (count / PAGE_SIZE) + 1;

        if (params.getSort() == null) {
            params.setSort(SortOrder.DESC);
        }
        String finalSql = sql + " ORDER BY timestamp " + params.getSort().name() + " LIMIT :limit OFFSET :offset ";
        ;
        return IntStream.range(0, (int) (pageCount))
                .mapToObj(i -> createQuery(finalSql, params, from, to, i))
                .flatMap(Query::getResultStream);
//        var query = em.createNativeQuery(sql, RecognitionEvent.class);
//        setQueryParams(params, from, to, query);

//        Stream<RecognitionEvent> result = (Stream<RecognitionEvent>) query.getResultStream();
//
//
//        return result;
    }

    private Query createQuery(String sql, FilterParams params, long from, long to, long pageNumber) {
        var query = em.createNativeQuery(sql, RecognitionEvent.class);
        setQueryParams(params, from, to, query);
        query.setParameter("limit", PAGE_SIZE);
        query.setParameter("offset", PAGE_SIZE * pageNumber);
        return query;
    }


    private void setQueryParams(FilterParams params, Pageable pageable, long from, long to, Query countQuery) {
        if (StringUtils.hasText(params.getGrnz())) {
            countQuery.setParameter("grnz", params.getGrnz());
        }
        if (params.getWeightMin() != null) {
            countQuery.setParameter("weightMin", params.getWeightMin());
        }
        if (params.getWeightMax() != null) {
            countQuery.setParameter("weightMax", params.getWeightMax());
        }
        if (params.getWidthMin() != null) {
            countQuery.setParameter("widthMin", params.getWidthMin());
        }
        if (params.getWidthMax() != null) {
            countQuery.setParameter("widthMax", params.getWidthMax());
        }
        if (params.getLengthMin() != null) {
            countQuery.setParameter("lengthMin", params.getLengthMin());
        }
        if (params.getLengthMax() != null) {
            countQuery.setParameter("lengthMax", params.getLengthMax());
        }
        if (params.getAxlesMin() != null) {
            countQuery.setParameter("axlesMin", params.getAxlesMin());
        }
        if (params.getAxlesMax() != null) {
            countQuery.setParameter("axlesMax", params.getAxlesMax());
        }
        if (params.getSendStatus() != null) {
            countQuery.setParameter("sendStatus", params.getSendStatus().name());
        }
        countQuery.setParameter("from", from);
        countQuery.setParameter("to", to);
        if (pageable.isPaged()) {
            countQuery.setParameter("limit", pageable.getPageSize());
            countQuery.setParameter("offset", pageable.getOffset());
        }
    }

    private void setQueryParams(FilterParams params, long from, long to, Query countQuery) {
        if (StringUtils.hasText(params.getGrnz())) {
            countQuery.setParameter("grnz", params.getGrnz());
        }
        if (params.getWeightMin() != null) {
            countQuery.setParameter("weightMin", params.getWeightMin());
        }
        if (params.getWeightMax() != null) {
            countQuery.setParameter("weightMax", params.getWeightMax());
        }
        if (params.getWidthMin() != null) {
            countQuery.setParameter("widthMin", params.getWidthMin());
        }
        if (params.getWidthMax() != null) {
            countQuery.setParameter("widthMax", params.getWidthMax());
        }
        if (params.getLengthMin() != null) {
            countQuery.setParameter("lengthMin", params.getLengthMin());
        }
        if (params.getLengthMax() != null) {
            countQuery.setParameter("lengthMax", params.getLengthMax());
        }
        if (params.getAxlesMin() != null) {
            countQuery.setParameter("axlesMin", params.getAxlesMin());
        }
        if (params.getAxlesMax() != null) {
            countQuery.setParameter("axlesMax", params.getAxlesMax());
        }
        if (params.getSendStatus() != null) {
            countQuery.setParameter("sendStatus", params.getSendStatus().name());
        }
        countQuery.setParameter("from", from);
        countQuery.setParameter("to", to);
    }

}
