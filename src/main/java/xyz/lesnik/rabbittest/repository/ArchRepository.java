package xyz.lesnik.rabbittest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import xyz.lesnik.rabbittest.entity.Arch;

/**
 * 31.01.2022
 * ArchRepository
 * 21:05
 */
@Repository
public interface ArchRepository extends JpaRepository<Arch, Long> {

}
