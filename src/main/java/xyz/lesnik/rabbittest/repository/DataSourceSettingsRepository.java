package xyz.lesnik.rabbittest.repository;

import org.springframework.data.repository.CrudRepository;
import xyz.lesnik.rabbittest.entity.DataSourceSettings;

import java.util.stream.Stream;

/**
 * 30.03.2021
 * DataSourceSettingsRepository
 * 08:53
 */
public interface DataSourceSettingsRepository extends CrudRepository<DataSourceSettings, Long> {
    Stream<DataSourceSettings> findAllBy();
}
