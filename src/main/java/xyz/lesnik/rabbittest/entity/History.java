package xyz.lesnik.rabbittest.entity;

import lombok.Builder;
import lombok.Data;

/**
 * 28.01.2021
 * History
 * 13:32
 */
@Data
@Builder
public class History {
    long timestamp;
    String username;
    String action;
}
