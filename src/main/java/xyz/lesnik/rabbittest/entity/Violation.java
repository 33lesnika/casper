package xyz.lesnik.rabbittest.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 19.12.2020
 * Violation
 * 20:37
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Violation {
    ViolationType type;
    String description;
    String details;
    double value;
}
