package xyz.lesnik.rabbittest.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

/**
 * 14.12.2020
 * Image
 * 22:27
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Image {
    UUID id;
    byte[] data;
}
