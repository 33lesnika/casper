package xyz.lesnik.rabbittest.entity;

/**
 * 19.12.2020
 * ViolationType
 * 20:39
 */
public enum ViolationType {
    NO_VIOLATION, WEIGHT_PER_AXIS, LENGTH, WIDTH, TYPE_NOT_FOUND;

    public String getDescription(){
        return switch (this){
            case WIDTH -> "Превышена максимальная ширина";
            case LENGTH -> "Превышена максимальная длина";
            case WEIGHT_PER_AXIS -> "Превышена максимальная общая масса";
            case TYPE_NOT_FOUND -> "Тип ТС не определён";
            case NO_VIOLATION -> "Без нарушений";
        };
    }
}
