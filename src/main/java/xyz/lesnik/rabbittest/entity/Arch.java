package xyz.lesnik.rabbittest.entity;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import xyz.lesnik.rabbittest.controller.dto.ArchCodePercentage;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 31.01.2022
 * Arch
 * 19:42
 */
@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@TypeDefs({
        @TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
})
public class Arch {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String name;
    @Type(type = "jsonb")
    List<String> archCodes = new ArrayList<>();
    double lat;
    double lon;
    double x;
    double y;

    @Transient
    double violationsPercentage;

    @Transient
    double noViolationsPercentage;

    @Transient
    List<ArchCodePercentage> archCodePercentages;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Arch arch = (Arch) o;
        return id != null && Objects.equals(id, arch.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
