package xyz.lesnik.rabbittest.entity;

import lombok.*;

import javax.persistence.*;

/**
 * 22.01.2021
 * VehicleType
 * 9:07
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(uniqueConstraints={
        @UniqueConstraint(columnNames = {"axles", "type"})
})
@EqualsAndHashCode(of = "id")
public class CarType {
    @Id
    Integer id;
    String name;
    int axles;
    int type;
    double weightLimit;
    double heightLimit;
    double lengthLimit;
    double widthLimit;
}
