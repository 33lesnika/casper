package xyz.lesnik.rabbittest.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * 30.03.2021
 * DataSourceSettings
 * 08:49
 */
@Entity
@Data
@Table(name = "settings")
public class DataSourceSettings {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    @Transient
    String status;
    @Column(nullable = false)
    String host;
    short port = 5672;
    @Column(nullable = false)
    String username = "guest";
    @Column(nullable = false)
    String password = "guest";
    String routingKey;
    String queue;
    String alias;
}
