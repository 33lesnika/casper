package xyz.lesnik.rabbittest.entity;

/**
 * 10.12.2021
 * SendStatus
 * 20:07
 */
public enum SendStatus {
    NOT_SENT, SENDING, SUCCESS, FAILURE, UNKNOWN
}
