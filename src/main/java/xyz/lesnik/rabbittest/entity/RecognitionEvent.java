package xyz.lesnik.rabbittest.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.springframework.scheduling.annotation.Scheduled;
import xyz.lesnik.rabbittest.model.RecognitionImage;

import javax.persistence.*;
import java.util.*;
import java.util.regex.Pattern;

/**
 * 14.12.2020
 * RecognitionEvent
 * 17:20
 */
@Data
@Entity
@Table(name = "recognition_event")
@NoArgsConstructor
@TypeDefs({
        @TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
})
public class RecognitionEvent {

    private static final Pattern p = Pattern.compile("^[0-9]{2,3}[A-Z]{2,3}(0[1-9]|1[0-7])$|^[A-Z]{1}[0-9]{3}[A-Z]{2,3}$|^[DTHMK][0-9]{6}$|^HC[0-9]{4}$|^CMD[0-9]{4}$|^UN[0-9]{3}$|^[0-9]{4}(0[1-9]|1[0-7])$|^[BCFAL][0-9]{4}(0[1-9]|1[0-8])$|");

    @Id
    @GeneratedValue
    UUID id;
    String name;
    String alias;
    @Transient
    String originalAlias;
    @JsonProperty("GRNZ")
    String grnz;
    @JsonProperty("correctedGRNZ")
    String correctedGrnz;
    String modifiedBy;
    Long modificationTimestamp;
    Long timestamp;
    Integer quality;
    UUID anprfrontId;
    UUID ovcfrontId;
    UUID platefrontId;
    UUID anprrearId;
    UUID platerearId;
    Double velocity;
    @JsonProperty("axlescount")
    Integer axlesCount;
    @JsonProperty("errorflag")
    String errorFlag;
    @JsonProperty("warningflag")
    Integer warningFlag;
    Integer direction;
    @JsonProperty("grossweight")
    Double grossWeight;
    @JsonProperty("classwim")
    Integer classWIM;
    @JsonProperty("classsick")
    String classSick;
    @JsonProperty("classKZ")
    Integer classKZ;
    @JsonProperty("wheelbase")
    Double wheelBase;
    @JsonProperty("vehiclelength")
    Double vehicleLength = 0.0;
    @JsonProperty("vehicleheight")
    Double vehicleHeight = 0.0;
    @JsonProperty("vehiclewidth")
    Double vehicleWidth = 0.0;
    @Type(type = "jsonb")
    Set<Violation> violations;
    @Type(type = "jsonb")
    CarType carType;
    @Type(type = "jsonb")
    List<History> history = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "event_id")
    @BatchSize(size = 150)
    List<Axle> axles = new ArrayList<>();

    @Enumerated(EnumType.STRING)
    SendStatus sendStatus = SendStatus.NOT_SENT;

    @Type(type = "jsonb")
    List<String> notSentReasons = new ArrayList<>();

    @Transient
    Boolean resident = Boolean.FALSE;

    @PostLoad
    private void postLoad() {
        if (this.grnz == null) {
            if(this.correctedGrnz != null){
                this.resident = p.matcher(this.correctedGrnz).matches();
            }
            return;
        }
        this.resident = p.matcher(this.grnz).matches();
    }
}
