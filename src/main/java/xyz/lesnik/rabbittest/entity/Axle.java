package xyz.lesnik.rabbittest.entity;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

/**
 * 17.05.2021
 * Axle
 * 20:49
 */
@Entity
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Axle implements Comparable<Axle>{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(nullable = false)
    double weight = 0.0d;

    @Column(nullable = false)
    double distance = 0.0d;

    @Column(nullable = false)
    int number = 0;

    @Override
    public int compareTo(Axle to) {
        return this.number - to.number;
    }
}
