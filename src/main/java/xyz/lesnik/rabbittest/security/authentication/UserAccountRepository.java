package xyz.lesnik.rabbittest.security.authentication;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;


public interface UserAccountRepository extends CrudRepository<UserAccount, Integer> {

    Optional<UserAccount> findByUsername(String username);
}
