package xyz.lesnik.rabbittest.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import xyz.lesnik.rabbittest.controller.dto.EventFilterResponse;
import xyz.lesnik.rabbittest.controller.dto.FilterParams;
import xyz.lesnik.rabbittest.entity.CarType;
import xyz.lesnik.rabbittest.entity.RecognitionEvent;
import xyz.lesnik.rabbittest.entity.Violation;
import xyz.lesnik.rabbittest.entity.ViolationType;
import xyz.lesnik.rabbittest.service.RecognitionEventHelper;
import xyz.lesnik.rabbittest.service.RecognitionEventService;
import xyz.lesnik.rabbittest.util.FixedTotalCountPage;
import xyz.lesnik.rabbittest.util.OneBasedPageRequest;

import java.util.Set;
import java.util.UUID;

/**
 * 14.12.2020
 * RecognitionEventController
 * 21:55
 */

@RestController
@RequestMapping("/api/event")
@Slf4j
public class RecognitionEventController {
    private final RecognitionEventService recognitionEventService;
    private final RecognitionEventHelper helper;

    @Autowired
    public RecognitionEventController(RecognitionEventService recognitionEventService, RecognitionEventHelper helper) {
        this.recognitionEventService = recognitionEventService;
        this.helper = helper;
    }

    public void updateArchNames(RecognitionEvent recognitionEvent) {
        helper.updateArchNames(recognitionEvent);
        if (recognitionEvent.getViolations() == null) {
            recognitionEvent.setViolations(Set.of(NO_VIOLATION));
        }
        if (recognitionEvent.getViolations().isEmpty()) {
            recognitionEvent.getViolations().add(NO_VIOLATION);
        }
    }

    @GetMapping
    Page<RecognitionEvent> get(
            @RequestParam(name = "page", required = false, defaultValue = "1") int page,
            @RequestParam(name = "size", required = false, defaultValue = "20") int size,
            FilterParams filterParams
    ) {
        log.info("Got filter: {}", filterParams);
        final PageRequest pageRequest = PageRequest.of(page - 1, size);
        Page<RecognitionEvent> search = search(filterParams, pageRequest);
        return substituteAlias(new FixedTotalCountPage<>(search.getContent(), OneBasedPageRequest.of(page, size), search.getTotalElements()));
    }

    private Page<RecognitionEvent> search(FilterParams filterParams, Pageable pageable) {
        if (filterParams != null && filterParams.getFrom() != null) {
            return recognitionEventService.getEvents(filterParams, pageable);
        }
        return recognitionEventService.getAll();
    }

    private final static Violation NO_VIOLATION = new Violation(ViolationType.NO_VIOLATION, "Без нарушений", "Без нарушений", 0d);

    private Page<RecognitionEvent> substituteAlias(Page<RecognitionEvent> page) {
        page.forEach(this::updateArchNames);
        return page;
    }

    @GetMapping("/{uuid}")
    RecognitionEvent getById(@PathVariable UUID uuid) {
        return recognitionEventService.get(uuid);
    }

    @PostMapping()
    RecognitionEvent create(@RequestBody RecognitionEvent recognitionEvent) {
        return recognitionEventService.create(recognitionEvent);
    }

    @PostMapping("/{uuid}/grnz")
    RecognitionEvent correctGrnz(@PathVariable UUID uuid, @RequestBody String correctedGrnz) {
        return recognitionEventService.correctGrnz(uuid, correctedGrnz);
    }

    @PostMapping("/{uuid}/resend")
    RecognitionEvent resend(@PathVariable UUID uuid) {
        final RecognitionEvent saved = recognitionEventService.send(uuid);
        return saved;
    }

    @PutMapping()
    RecognitionEvent update(@RequestBody RecognitionEvent recognitionEvent) {
        return recognitionEventService.update(recognitionEvent);
    }

    @PutMapping("{id}/car_type")
    RecognitionEvent update(@PathVariable UUID id, @RequestBody CarType carType) {
        return recognitionEventService.changeCarType(id, carType);
    }

    @DeleteMapping("/{uuid}")
    void delete(@PathVariable UUID uuid) {
        recognitionEventService.delete(uuid);
    }

    @GetMapping("/filter-data")
    public EventFilterResponse getFilterData() {
        return recognitionEventService.getFilterData();
    }
}
