package xyz.lesnik.rabbittest.controller;

import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;
import xyz.lesnik.rabbittest.controller.dto.FilterParams;
import xyz.lesnik.rabbittest.controller.dto.PercentageReportResponse;
import xyz.lesnik.rabbittest.controller.dto.SummaryReportRequest;
import xyz.lesnik.rabbittest.controller.dto.SummaryReportResponse;
import xyz.lesnik.rabbittest.service.ReportService;

import javax.servlet.http.HttpServletResponse;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;

/**
 * 30.01.2021
 * ReportController
 * 12:40
 */
@RestController
@RequestMapping("/api/report")
public class ReportController {

    private final ReportService reportService;

    public ReportController(ReportService reportService) {
        this.reportService = reportService;
    }

    @GetMapping("/events")
    StreamingResponseBody getReport(FilterParams filterParams,
                                    HttpServletResponse response
    ) {
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        ContentDisposition attachment = ContentDisposition
                .builder("attachment")
                .filename( "report-" + LocalDateTime.now() + ".xlsx", StandardCharsets.UTF_8).build();
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, attachment.toString());
        return outputStream -> {
            reportService.writeContent(filterParams, outputStream);
        };
    }

    @GetMapping("/summary")
    SummaryReportResponse getSummaryReport(SummaryReportRequest summaryReportRequest) {
        return reportService.generateSummaryReport(summaryReportRequest);
    }

    @GetMapping("/percentage")
    PercentageReportResponse getSummaryReportPercentage(SummaryReportRequest summaryReportRequest) {
        return reportService.generatePercentageReport(summaryReportRequest);
    }
}
