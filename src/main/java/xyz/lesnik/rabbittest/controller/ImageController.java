package xyz.lesnik.rabbittest.controller;

import org.springframework.web.bind.annotation.*;
import xyz.lesnik.rabbittest.entity.Image;
import xyz.lesnik.rabbittest.service.ImageService;

import java.util.Base64;
import java.util.UUID;

/**
 * 14.12.2020
 * ImageController
 * 23:15
 */

@RestController
@RequestMapping("/api/image")
public class ImageController {
    private final ImageService imageService;

    public ImageController(ImageService imageService) {
        this.imageService = imageService;
    }

    @GetMapping("/{uuid}")
    public byte[] get(@PathVariable UUID uuid) {
        return imageService.get(uuid).getData();
    }

    @PostMapping
    public Image create(@RequestBody Image image) {
        return imageService.create(image);
    }

    @DeleteMapping()
    void delete(@RequestBody Image image){
        imageService.delete(image);
    }
}
