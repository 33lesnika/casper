package xyz.lesnik.rabbittest.controller.dto;

/**
 * 01.02.2022
 * ArchStatistics
 * 21:55
 */
public interface ArchStatistics {
    String getName();
    long getNoViolationsCount();
    long getAllCount();
}
