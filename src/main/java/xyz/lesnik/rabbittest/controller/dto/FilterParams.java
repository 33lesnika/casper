package xyz.lesnik.rabbittest.controller.dto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import xyz.lesnik.rabbittest.entity.SendStatus;
import xyz.lesnik.rabbittest.entity.ViolationType;

/**
 * 27.04.2021
 * FilterParams
 * 01:11
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class FilterParams {
    Integer weightMin;
    Integer weightMax;
    Integer widthMin;
    Integer widthMax;
    Integer lengthMin;
    Integer lengthMax;
    Integer axlesMin;
    Integer axlesMax;
    Long from;
    Long to;
    Integer carType;
    ViolationType violationType;
    String alias;
    SortOrder sort;
    SendStatus sendStatus;
    String grnz;
}
