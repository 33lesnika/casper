package xyz.lesnik.rabbittest.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 23.04.2021
 * CarTypeResponse
 * 19:12
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CarTypeResponse {
    Integer id;
    String name;
}
