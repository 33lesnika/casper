package xyz.lesnik.rabbittest.controller.dto;

/**
 * 11.12.2021
 * SummaryCountsInterface
 * 22:54
 */
public interface SummaryCountsInterface {
    String getArch();
    long getOverweightCount();
    long getNoViolationsCount();
    long getAllCount();
    long getSentCount();
}
