package xyz.lesnik.rabbittest.controller.dto;

import lombok.Data;

/**
 * 01.02.2022
 * ArchCodePercentage
 * 22:41
 */
@Data
public class ArchCodePercentage {
    String code;
    double violationPercentage;
    double noViolationPercentage;
}
