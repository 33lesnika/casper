package xyz.lesnik.rabbittest.controller.dto;

import lombok.Data;

/**
 * 11.12.2021
 * SummaryReportRequest
 * 20:49
 */
@Data
public class SummaryReportRequest {
    long from;
    long to;
}
