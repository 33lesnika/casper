package xyz.lesnik.rabbittest.controller.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 11.12.2021
 * SummaryReportResponse
 * 20:06
 */
@Data
public class SummaryReportResponse {
    long from;
    long to;
    SummaryCountsInterface totalCounts;
    List<SummaryCountsInterface> archCounts = new ArrayList<>();
}
