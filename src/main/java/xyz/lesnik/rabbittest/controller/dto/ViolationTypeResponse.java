package xyz.lesnik.rabbittest.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 23.04.2021
 * ViolationType
 * 19:11
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ViolationTypeResponse {
    String type;
    String description;
}
