package xyz.lesnik.rabbittest.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 11.12.2021
 * SummaryCounts
 * 21:54
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SummaryCounts implements SummaryCountsInterface{
    String arch;
    long overweightCount = 0;
    long noViolationsCount = 0;
    long allCount = 0;
    long sentCount = 0;
}
