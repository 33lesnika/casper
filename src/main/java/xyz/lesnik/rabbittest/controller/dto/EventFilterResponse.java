package xyz.lesnik.rabbittest.controller.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 23.04.2021
 * EventFilterResponse
 * 19:09
 */
@Data
public class EventFilterResponse {
    List<ViolationTypeResponse> violationList = new ArrayList<>();
    List<CarTypeResponse> carTypeList = new ArrayList<>();
    List<String> aliasList = new ArrayList<>();

}
