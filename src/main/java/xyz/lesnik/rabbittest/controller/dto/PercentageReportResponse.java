package xyz.lesnik.rabbittest.controller.dto;

import lombok.Data;

/**
 * 31.01.2022
 * PercentageReportResponse
 * 19:00
 */
@Data
public class PercentageReportResponse {
    long from;
    long to;
    double violationPercentage;
    double normalPercentage;
}
