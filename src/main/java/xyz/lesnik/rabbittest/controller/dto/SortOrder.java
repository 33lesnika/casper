package xyz.lesnik.rabbittest.controller.dto;

/**
 * 27.04.2021
 * SortOrder
 * 18:28
 */
public enum SortOrder {
    ASC, DESC
}
