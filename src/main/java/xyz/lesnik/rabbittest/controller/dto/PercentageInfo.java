package xyz.lesnik.rabbittest.controller.dto;

/**
 * 31.01.2022
 * PercentageInfo
 * 19:06
 */
public interface PercentageInfo {
    long getAllCount();
    long getNoViolationsCount();
}
