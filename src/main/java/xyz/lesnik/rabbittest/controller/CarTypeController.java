package xyz.lesnik.rabbittest.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.lesnik.rabbittest.entity.CarType;
import xyz.lesnik.rabbittest.service.CarTypeService;

import java.util.Collection;

/**
 * 28.01.2021
 * DictionaryController
 * 12:13
 */
@RestController
@RequestMapping("/api/car_type")
public class CarTypeController {

    private final CarTypeService carTypeService;

    public CarTypeController(CarTypeService carTypeService) {
        this.carTypeService = carTypeService;
    }

    @GetMapping()
    public Collection<CarType> get() {
        return carTypeService.getAll();
    }
}
