package xyz.lesnik.rabbittest.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import xyz.lesnik.rabbittest.controller.dto.ArchData;
import xyz.lesnik.rabbittest.entity.Arch;
import xyz.lesnik.rabbittest.service.ArchService;

import java.util.List;

/**
 * 31.01.2022
 * ArchController
 * 21:06
 */
@RestController
@RequestMapping("/api/arch")
public class ArchController {

    private final ArchService archService;

    public ArchController(ArchService archService) {
        this.archService = archService;
    }

    @GetMapping
    public List<Arch> getArches(@RequestParam(defaultValue = "0") Long from, @RequestParam(defaultValue = "-1") Long to){
        return archService.getArchData(from, to);
    }
}
