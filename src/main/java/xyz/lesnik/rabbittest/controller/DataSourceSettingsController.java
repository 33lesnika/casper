package xyz.lesnik.rabbittest.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import xyz.lesnik.rabbittest.entity.DataSourceSettings;
import xyz.lesnik.rabbittest.service.DataSourceSettingsService;

import java.util.Collection;
import java.util.stream.Collectors;

/**
 * 30.03.2021
 * DataSourceSettingsController
 * 09:46
 */
@RestController
@RequestMapping("/api/config")
@Slf4j
public class DataSourceSettingsController {
    private final DataSourceSettingsService service;

    public DataSourceSettingsController(DataSourceSettingsService service) {
        this.service = service;
    }

    @GetMapping
    public Collection<DataSourceSettings> getAll(){
        return service.getAll().stream().peek(this::hidePassword).collect(Collectors.toList());
    }

    @PostMapping
    public DataSourceSettings create(@RequestBody DataSourceSettings settings){
        log.info("Got settings: {}", settings);
        var response = service.create(settings);
        hidePassword(response);
        return response;
    }

    @PutMapping("/{id}")
    public DataSourceSettings update(@PathVariable Long id, @RequestBody DataSourceSettings settings){
        var update = service.update(id, settings);
        hidePassword(update);
        return update;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id){
        service.delete(id);
    }

    private void hidePassword(DataSourceSettings settings){
        settings.setPassword(null);
    }

}
