create table if not exists user_account(
    id serial primary key,
    account_non_expired boolean not null default true,
    account_non_locked boolean not null default true,
    credentials_non_expired boolean not null default true,
    enabled boolean not null default true,
    username varchar not null,
    password varchar not null,
    fio varchar not null ,
    auth varchar not null
);

