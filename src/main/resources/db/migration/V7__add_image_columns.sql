alter table recognition_event rename column photo_plate_id to anprfront_id;
alter table recognition_event add column ovcfront_id uuid;
alter table recognition_event add column platefront_id uuid;
alter table recognition_event add column anprrear_id uuid;
alter table recognition_event add column platerear_id uuid;
alter table recognition_event drop column model_id;
