ALTER TABLE recognition_event ADD COLUMN not_sent_reasons jsonb not null default '[]'::jsonb;
