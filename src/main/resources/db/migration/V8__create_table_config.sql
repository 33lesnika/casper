create table if not exists settings
(
    id          serial primary key,
    host        varchar not null,
    port        numeric not null default 5672,
    username    varchar not null,
    password    varchar not null,
    routing_key varchar,
    queue       varchar not null,
    alias       varchar
);
