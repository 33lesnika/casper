create table if not exists car_type(
    id numeric PRIMARY KEY,
    name varchar not null default '',
    axles numeric NOT NULL,
    type numeric NOT NULL,
    weight_limit numeric not null default 0,
    height_limit numeric not null default 0,
    length_limit numeric not null default 0,
    width_limit numeric not null default 0,

    UNIQUE (axles, type)
)
