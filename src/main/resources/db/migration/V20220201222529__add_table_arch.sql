create table if not exists arch
(
    id         serial primary key,
    name       varchar not null,
    arch_codes jsonb   not null default '[]'::jsonb,
    lat        numeric not null default 0,
    lon        numeric not null default 0,
    x          numeric not null default 0,
    y          numeric not null default 0
);

