ALTER TABLE recognition_event ADD COLUMN corrected_grnz varchar(15);
ALTER TABLE recognition_event ADD COLUMN modified_by varchar;
ALTER TABLE recognition_event ADD COLUMN modification_timestamp numeric;
ALTER TABLE recognition_event ADD COLUMN send_status varchar;
