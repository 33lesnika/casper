create table if not exists recognition_event (
    id UUID PRIMARY KEY,
    name varchar(20),
    alias varchar(255),
    grnz varchar(15),
    timestamp numeric,
    quality numeric,
    photo_plate_id uuid,
    model_id uuid,
    velocity numeric,
    axles_count numeric,
    error_flag varchar,
    warning_flag numeric,
    direction numeric,
    gross_weight numeric,
    classwim numeric,
    class_sick varchar,
    classkz numeric,
    wheel_base numeric,
    vehicle_length numeric,
    vehicle_height numeric,
    vehicle_width numeric
)
