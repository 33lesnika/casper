create table if not exists axle
(
    id       serial primary key,
    weight   double precision not null default 0,
    distance double precision not null default 0,
    number   smallint         not null default 0,
    event_id UUID,
    CONSTRAINT fk_recognition_events
        FOREIGN KEY (event_id)
            REFERENCES recognition_event (id)
);
