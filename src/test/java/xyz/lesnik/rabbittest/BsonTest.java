package xyz.lesnik.rabbittest;

import com.rabbitmq.client.*;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.bson.RawBsonDocument;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.TimeoutException;

/**
 * 07.06.2021
 * BsonTest
 * 23:42
 */
@Slf4j
public class BsonTest {
    @SneakyThrows
    @Test
    public void testBson() {
        asdf();
        Thread.sleep(10000);
    }

    private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }

    public void asdf() {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUsername("sais");
        factory.setPassword("sais$2021");
        factory.setHost("10.0.127.115");
        factory.setPort(5672);
        try {
            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();
//            String queueName = "wim-bson-baiserke";
            String queueName = "wim-bson-a01b54p3";
//        channel.queueBind(queueName, cc.getExchange(), cc.getRoutingKey());
            channel.basicConsume(queueName, false, "casperConsumerTag" + queueName,
                    new DefaultConsumer(channel) {
                        @Override
                        public void handleDelivery(String consumerTag,
                                                   Envelope envelope,
                                                   AMQP.BasicProperties properties,
                                                   byte[] body)
                                throws IOException {
                            long deliveryTag = envelope.getDeliveryTag();
                            RawBsonDocument doc = new RawBsonDocument(body);
                            log.info("Document: {}", doc.toJson());
                            Files.writeString(Path.of("files/" + System.currentTimeMillis()+".json"), doc.toJson());
                            channel.basicAck(deliveryTag, false);
//                            try {
//                                channel.close();
//                                connection.close();
//                            } catch (TimeoutException e) {
//                                e.printStackTrace();
//                            }
                        }
                    });
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }

    }
}
