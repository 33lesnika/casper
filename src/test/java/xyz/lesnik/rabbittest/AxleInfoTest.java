package xyz.lesnik.rabbittest;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.ActiveProfiles;
import xyz.lesnik.rabbittest.entity.Axle;
import xyz.lesnik.rabbittest.entity.RecognitionEvent;
import xyz.lesnik.rabbittest.service.RecognitionEventService;

import javax.transaction.Transactional;
import java.util.Comparator;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * 17.05.2021
 * AxleInfoTest
 * 21:10
 */
@SpringBootTest
@ActiveProfiles("local")
@Slf4j
public class AxleInfoTest {

    @Autowired
    private RecognitionEventService recognitionEventService;

    @Test
    @Transactional
    public void afterLoad(){
        RecognitionEvent event = new RecognitionEvent();
        event.setGrnz("VOVAN228");
        Axle axle0 = new Axle();
        axle0.setDistance(0);
        axle0.setNumber(0);
        axle0.setWeight(958.0d);
        event.getAxles().add(axle0);
        Axle axle1 = new Axle();
        axle1.setDistance(5.5d);
        axle1.setNumber(1);
        axle1.setWeight(1024.5d);
        event.getAxles().add(axle1);
        event.getAxles().sort(Comparator.naturalOrder());
        event.setAxlesCount(event.getAxles().size());
        event.setGrossWeight(event.getAxles().stream().map(Axle::getWeight).reduce(Double::sum).orElse(0.0d));
        event.setClassWIM(0);
        log.debug("Event: {}",event);
        var saved = recognitionEventService.create(event);
        axle0.setId(saved.getAxles().get(0).getId());
        axle1.setId(saved.getAxles().get(1).getId());
        assertThat(saved.getAxles().get(0)).isEqualTo(axle0);
        assertThat(saved.getAxles().get(1)).isEqualTo(axle1);
    }
}
