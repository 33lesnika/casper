package xyz.lesnik.rabbittest;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import xyz.lesnik.rabbittest.entity.Image;
import xyz.lesnik.rabbittest.entity.RecognitionEvent;
import xyz.lesnik.rabbittest.service.ImageService;
import xyz.lesnik.rabbittest.soap.ArchRecord;
import xyz.lesnik.rabbittest.soap.ArchRecordRequest;
import xyz.lesnik.rabbittest.vshep.ArchRecordMapper;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

/**
 * 08.09.2021
 * ArchRecordTest
 * 15:59
 */
public class ArchRecordTest {

    String underTest = "{\n" +
            "  \"id\": \"b5af8d25-2b54-4da0-896d-333ad249c81b\",\n" +
            "  \"name\": \"A01B54R1P02L01\",\n" +
            "  \"alias\": \"Нур-Султан-Кабанбай Батыра-Темиртау, км 22\",\n" +
            "  \"timestamp\": 1614168855118,\n" +
            "  \"quality\": 52,\n" +
            "  \"anprfrontId\": \"2edf885b-88d8-356a-95c8-e7a68e3f94dd\",\n" +
            "  \"ovcfrontId\": \"fabb488a-8203-3c9b-b8f2-b2da97e4e57d\",\n" +
            "  \"platefrontId\": \"b2033fa3-637f-397f-9cd3-2eabbaf9f5a2\",\n" +
            "  \"anprrearId\": \"a6960b06-9bb0-3ec1-a18c-9a251b711567\",\n" +
            "  \"platerearId\": \"2391d822-c771-374d-a386-77efd5e911ae\",\n" +
            "  \"velocity\": 77.5,\n" +
            "  \"direction\": 0,\n" +
            "  \"violations\": [\n" +
            "    {\n" +
            "      \"type\": \"TYPE_NOT_FOUND\",\n" +
            "      \"description\": \"Тип ТС не определён\",\n" +
            "      \"details\": \"Нет данных по типу ТС с количестом осей 2, класс 12\"\n" +
            "    }\n" +
            "  ],\n" +
            "  \"carType\": null,\n" +
            "  \"history\": [],\n" +
            "  \"GRNZ\": \"411CDZ01\",\n" +
            "  \"axlescount\": 2,\n" +
            "  \"errorflag\": \"0\",\n" +
            "  \"warningflag\": 32,\n" +
            "  \"grossweight\": 4320,\n" +
            "  \"classwim\": 12,\n" +
            "  \"classsick\": null,\n" +
            "  \"classKZ\": null,\n" +
            "  \"wheelbase\": 4.34,\n" +
            "  \"vehiclelength\": 0,\n" +
            "  \"vehicleheight\": 0,\n" +
            "  \"vehiclewidth\": 0\n" +
            "}";

    @SneakyThrows
    @Test
    public void testXml() {
        ImageService imageService = Mockito.mock(ImageService.class);
        Mockito.when(imageService.get(Mockito.any(UUID.class))).thenReturn(new Image(UUID.randomUUID(), new byte[]{}));
        ArchRecordMapper archMapper = new ArchRecordMapper(imageService);
        ObjectMapper mapper = new ObjectMapper();
        var event = mapper.readValue(underTest, RecognitionEvent.class);
        final ArchRecord record = archMapper.toRecord(event);
        ArchRecordRequest request = new ArchRecordRequest();
        request.setRecord(record);
        final String v1 = jaxbObjectToXML(request);
        System.out.println(v1);
        String envelope = renderWith(Map.of("archRequest", v1));
//        System.out.println(envelope);
    }

    public String renderWith(Map<String, String> modelMap) {
        if (modelMap == null || modelMap.isEmpty()) {
            return template;
        }
        String copy = template;
        for (Map.Entry<String, String> entry : modelMap.entrySet()) {
            String k = entry.getKey();
            String v = entry.getValue();
            if (v == null) {
                v = "";
            }
            copy = copy.replaceAll("\\{\\{" + k + "\\}\\}", v);
        }
        return copy;
    }

    private static String jaxbObjectToXML(ArchRecordRequest request) {
        String xmlString = "";
        try {
            JAXBContext context = JAXBContext.newInstance(ArchRecordRequest.class);
            Marshaller m = context.createMarshaller();

            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE); // To format XML

            StringWriter sw = new StringWriter();
            m.marshal(request, sw);
            xmlString = sw.toString();

        } catch (JAXBException e) {
            e.printStackTrace();
        }

        return xmlString;
    }


    String template =
            """
                    <?xml version='1.0' encoding='UTF-8'?>
                    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                                      xmlns:typ="http://bip.bee.kz/SyncChannel/v10/Types">
                        <soapenv:Header/>
                        <soapenv:Body>
                            <typ:SendMessage>
                                <request>
                                    <requestInfo>
                                        <messageId>{{message.id}}</messageId>
                                        <serviceId>KTK_ARCH_RECORD_SYNC</serviceId>
                                        <routeId/>
                                        <messageDate>{{message.date}}</messageDate>
                                        <sender>
                                            <senderId>iastbd</senderId>
                                            <password>iastbd</password>
                                        </sender>
                                    </requestInfo>
                                    <requestData>
                                        {{archRequest}}
                                    </requestData>
                                </request>
                            </typ:SendMessage>
                        </soapenv:Body>
                    </soapenv:Envelope>        
                    """;

    @Test
    public void yesy2() {
        RecognitionEvent event = new RecognitionEvent();
        final String given = "A01B54R1P04L02";
        event.setName(given);
        final String archCode = event.getName().length() < 3 ?
                event.getName() :
                event.getName().substring(0, event.getName().length() - 3);
        System.out.println(given);
        System.out.println(archCode);
    }

    @Test
    public void formatTest() {
        System.out.println(String.format("%.2f", 1.01));
        DecimalFormat df = new DecimalFormat("#.00",
                DecimalFormatSymbols.getInstance(Locale.US));
        System.out.println(df.format(1.01));
    }
}
