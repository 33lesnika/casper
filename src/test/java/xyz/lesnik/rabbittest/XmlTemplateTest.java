package xyz.lesnik.rabbittest;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import xyz.lesnik.rabbittest.entity.RecognitionEvent;
import xyz.lesnik.rabbittest.entity.ViolationType;
import xyz.lesnik.rabbittest.vshep.NewXmlSigner;
import xyz.lesnik.rabbittest.vshep.TemplateXmlView;
import xyz.lesnik.rabbittest.vshep.XmlSigner;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPMessage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * 25.02.2021
 * XmlTemplateTest
 * 07:29
 */
public class XmlTemplateTest {

    String underTest = "{\n" +
            "  \"id\": \"b5af8d25-2b54-4da0-896d-333ad249c81b\",\n" +
            "  \"name\": \"A01B54R1P02L01\",\n" +
            "  \"alias\": \"Нур-Султан-Кабанбай Батыра-Темиртау, км 22\",\n" +
            "  \"timestamp\": 1614168855118,\n" +
            "  \"quality\": 52,\n" +
            "  \"anprfrontId\": \"2edf885b-88d8-356a-95c8-e7a68e3f94dd\",\n" +
            "  \"ovcfrontId\": \"fabb488a-8203-3c9b-b8f2-b2da97e4e57d\",\n" +
            "  \"platefrontId\": \"b2033fa3-637f-397f-9cd3-2eabbaf9f5a2\",\n" +
            "  \"anprrearId\": \"a6960b06-9bb0-3ec1-a18c-9a251b711567\",\n" +
            "  \"platerearId\": \"2391d822-c771-374d-a386-77efd5e911ae\",\n" +
            "  \"velocity\": 77.5,\n" +
            "  \"direction\": 0,\n" +
            "  \"violations\": [\n" +
            "    {\n" +
            "      \"type\": \"TYPE_NOT_FOUND\",\n" +
            "      \"description\": \"Тип ТС не определён\",\n" +
            "      \"details\": \"Нет данных по типу ТС с количестом осей 2, класс 12\"\n" +
            "    }\n" +
            "  ],\n" +
            "  \"carType\": null,\n" +
            "  \"history\": [],\n" +
            "  \"GRNZ\": \"411CDZ01\",\n" +
            "  \"axlescount\": 2,\n" +
            "  \"errorflag\": \"0\",\n" +
            "  \"warningflag\": 32,\n" +
            "  \"grossweight\": 4320,\n" +
            "  \"classwim\": 12,\n" +
            "  \"classsick\": null,\n" +
            "  \"classKZ\": null,\n" +
            "  \"wheelbase\": 4.34,\n" +
            "  \"vehiclelength\": 0,\n" +
            "  \"vehicleheight\": 0,\n" +
            "  \"vehiclewidth\": 0\n" +
            "}";

    String template = "<?xml version='1.0' encoding='UTF-8'?>\n" +
            "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"\n" +
            "                  xmlns:typ=\"http://bip.bee.kz/SyncChannel/v10/Types\">\n" +
            "    <soapenv:Header/>\n" +
            "    <soapenv:Body>\n" +
            "        <typ:SendMessage>\n" +
            "            <request>\n" +
            "                <requestInfo>\n" +
            "                    <messageId>bd4713a4-101b-4865-8fec-62afc5df88f1</messageId>\n" +
            "                    <correlationId>3242352365354356</correlationId>\n" +
            "                    <serviceId>KTK_ARCH_RECORD_SYNC</serviceId>\n" +
            "                    <routeId/>\n" +
            "                    <messageDate>2020-12-18T12:54:26.291+06:00</messageDate>\n" +
            "                    <sender>\n" +
            "                        <senderId>iastbd</senderId>\n" +
            "                        <password>iastbd</password>\n" +
            "                    </sender>\n" +
            "                </requestInfo>\n" +
            "                <requestData>\n" +
            "                    <data xmlns:ns2=\"http://www.ktk.kz/archControl/v1\"\n" +
            "                          xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"ns2:archRecord\">\n" +
            "                        <archCode>{{arch.code}}</archCode>\n" +
            "                        <createdBy>{{createdBy}}</createdBy>\n" +
            "                        <softwareVersion>{{software.version}}</softwareVersion>\n" +
            "                        <credence>{{credence}}</credence>\n" +
            "                        <wayType>{{way.type}}</wayType>\n" +
            "                        <direction>{{direction}}</direction>\n" +
            "                        <lane>{{lane}}</lane>\n" +
            "                        <recordDate>{{recordDate}}</recordDate>\n" +
            "                        <vehicleNumber>{{vehicle.number}}</vehicleNumber>\n" +
            "                        <vehicleCountry>{{vehicle.country}}</vehicleCountry>\n" +
            "                        <vehicleNumberRear>{{vehicle.numberRear}}</vehicleNumberRear>\n" +
            "                        <hasVehicleNumberPhoto>{{hasVehicleNumberPhoto}}</hasVehicleNumberPhoto>\n" +
            "                        <vehicleType>{{vehicle.type}}</vehicleType>\n" +
            "                        <uncorrectType>{{uncorrectType}}</uncorrectType>\n" +
            "                        <speed>{{speed}}</speed>\n" +
            "                        <length>{{length}}</length>\n" +
            "                        <width>{{width}}</width>\n" +
            "                        <height>{{height}}</height>\n" +
            "                        <wheelBase>{{wheelBase}}</wheelBase>\n" +
            "                        <weight>{{weight}}</weight>\n" +
            "                        <weightLimit>{{weightLimit}}</weightLimit>\n" +
            "                        <overWeight>{{overWeight}}</overWeight>\n" +
            "                        <isOverweightGross>{{isOverweightGross}}</isOverweightGross>\n" +
            "                        <isOverweightPartial>{{isOverweightPartial}}</isOverweightPartial>\n" +
            "                        <isOverweight>{{isOverweight}}</isOverweight>\n" +
            "                        <isExceededWidth>{{isExceededWidth}}</isExceededWidth>\n" +
            "                        <isExceededHeight>{{isExceededHeight}}</isExceededHeight>\n" +
            "                        <isExceededLength>{{isExceededLength}}</isExceededLength>\n" +
            "                        <isOversized>{{isOversized}}</isOversized>\n" +
            "                        <isWrongDirection>{{isWrongDirection}}</isWrongDirection>\n" +
            "                        <isNonStandard>{{isNonStandard}}</isNonStandard>\n" +
            "                        <isExceededOperatingRange>{{isExceededOperatingRange}}</isExceededOperatingRange>\n" +
            "                        <signature>{{signature}}</signature>\n" +
            "                        <countryCode>{{countryCode}}</countryCode>\n" +
            "                    </data>\n" +
            "                </requestData>\n" +
            "            </request>\n" +
            "        </typ:SendMessage>\n" +
            "    </soapenv:Body>\n" +
            "</soapenv:Envelope>\n";

    @SneakyThrows
    @Test
    public void toXml() {
        ObjectMapper mapper = new ObjectMapper();
        var event = mapper.readValue(underTest, RecognitionEvent.class);
        TemplateXmlView view = new TemplateXmlView(template);
        System.out.println(view.renderWith(toModel(event)));
    }


    public Map<String, String> toModel(RecognitionEvent event) {
        Map<String, String> model = new HashMap<>();
        model.put("arch.code", "234");
        model.put("createdBy", "06010102-3000-4250-0000-000000000011");
        model.put("software.version", "2.3.1.0");
        model.put("credence", "0");
        model.put("way.type", String.valueOf(event.getDirection()));
        model.put("direction", String.valueOf(event.getDirection() + 1));
        model.put("lane", "1");
        model.put("recordDate", ZonedDateTime.ofInstant(Instant.ofEpochMilli(event.getTimestamp()), ZoneId.systemDefault()).toString());
        model.put("vehicle.number", event.getGrnz());
        model.put("vehicle.country", "KAZ");
        model.put("vehicle.numberRear", event.getGrnz());
        model.put("hasVehicleNumberPhoto", "false");
        if (event.getPlatefrontId() != null) {
            model.put("hasVehicleNumberPhoto", "true");
        }
        model.put("vehicle.type", String.valueOf(event.getClassWIM()));

        model.put("uncorrectType", "0");
        model.put("speed", String.format("%.2f", event.getVelocity()));
        model.put("length", String.valueOf(event.getVehicleLength() * 100));
        model.put("width", String.valueOf(event.getVehicleWidth() * 100));
        model.put("height", String.valueOf(event.getVehicleHeight() * 100));
        model.put("wheelBase", String.valueOf(event.getWheelBase() * 100));
        model.put("weight", String.valueOf(event.getGrossWeight().intValue()));
        model.put("weightLimit", "0");
        model.put("overWeight", "0");
        var isOverweight = String.valueOf(event.getViolations().stream().anyMatch(x -> x.getType() == ViolationType.WEIGHT_PER_AXIS));
        model.put("isOverweightGross", isOverweight);
        model.put("isOverweightPartial", "false");
        model.put("isOverweight", isOverweight);
        var isExceededWidth = event.getViolations().stream().anyMatch(x -> x.getType() == ViolationType.WIDTH);
        model.put("isExceededWidth", String.valueOf(isExceededWidth));
        var isExceededHeight = false;
        model.put("isExceededHeight", String.valueOf(isExceededHeight));
        var isExceededLength = event.getViolations().stream().anyMatch(x -> x.getType() == ViolationType.LENGTH);
        model.put("isExceededLength", String.valueOf(isExceededLength));
        var isOversized = isExceededHeight || isExceededLength || isExceededWidth;
        model.put("isOversized", String.valueOf(isOversized));
        model.put("isWrongDirection", "false");
        model.put("isNonStandard", "false");
        model.put("isExceededOperatingRange", "false");
        model.put("countryCode", "KAZ");
        model.put("signature", "");
        return model;
    }


    @SneakyThrows
    @Test
    public void toXmlSigned() {
        ObjectMapper mapper = new ObjectMapper();
        var event = mapper.readValue(underTest, RecognitionEvent.class);
        TemplateXmlView view = new TemplateXmlView(template);
        XmlSigner signer = new XmlSigner("GOSTKNCA_44fca439227560084e007115e8539a5777ab7bc6.p12", "Aa123456");
//        System.out.println(signer.sign(view.renderWith(toModel(event))));
    }

    @SneakyThrows
    @Test
    public void toXmlSignedNew() {
        ObjectMapper mapper = new ObjectMapper();
        var event = mapper.readValue(underTest, RecognitionEvent.class);
        TemplateXmlView view = new TemplateXmlView(template);
        NewXmlSigner signer = new NewXmlSigner("GOSTKNCA_44fca439227560084e007115e8539a5777ab7bc6.p12", "Aa123456");
        String envelope = view.renderWith(toModel(event));
        InputStream is = new ByteArrayInputStream(envelope.getBytes());
        SOAPMessage msg = MessageFactory.newInstance().createMessage(null, is);
        signer.addHeadSecurity(msg);
        String signedXml = signer.convertMessageToString(msg);
        System.out.println(signedXml);
    }
}
