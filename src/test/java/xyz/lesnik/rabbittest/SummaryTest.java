package xyz.lesnik.rabbittest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import xyz.lesnik.rabbittest.controller.dto.SummaryCounts;
import xyz.lesnik.rabbittest.controller.dto.SummaryReportResponse;

import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;

/**
 * 11.12.2021
 * SummaryTest
 * 21:55
 */
public class SummaryTest {
    @SneakyThrows
    @Test
    public void test(){
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModules(new JavaTimeModule());
        SummaryReportResponse response = new SummaryReportResponse();
        response.setFrom(Instant.now().toEpochMilli());
        response.setTo(Instant.now().plus(1, ChronoUnit.HOURS).toEpochMilli());
        SummaryCounts total = new SummaryCounts();
        response.setTotalCounts(total);
        total.setAllCount(10);
        total.setNoViolationsCount(8);
        total.setOverweightCount(2);
        total.setSentCount(1);
        SummaryCounts arch1 = new SummaryCounts();
        response.getArchCounts().add(arch1);
        arch1.setArch("arch1");
        SummaryCounts arch2 = new SummaryCounts();
        response.getArchCounts().add(arch2);
        arch2.setArch("arch2");
        Files.write(Path.of("summary.json"), mapper.writeValueAsBytes(response));
    }

}
